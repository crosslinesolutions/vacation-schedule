﻿namespace Mobile.Core
{
    public static class MessageCodeHandling
    {
        public static string GetText(MessageCode messageCode)
        {
            return messageCode switch
            {
                MessageCode.Ok => "Операция выполнена",
                MessageCode.UndefinedError => "Произошла ошибка 'UndefinedError'",
                MessageCode.UndefinedHttpError => "Произошла ошибка 'UndefinedHttpError'",
                MessageCode.UndefinedNetworkError => "Произошла ошибка 'UndefinedNetworkError'",
                MessageCode.BadRequest => "Произошла ошибка 'BadRequest'",
                MessageCode.Unauthorized => "Неавторизованный пользователь",
                _ => "Неизвестно что случилось. Повторите",
            };
        }
    }
}
