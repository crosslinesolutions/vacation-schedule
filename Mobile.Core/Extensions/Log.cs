﻿using System.IO;
using System.IO.IsolatedStorage;
using System.Threading;
using System.Threading.Tasks;

namespace System
{
    public static class Log
    {
        private static readonly SemaphoreSlim OneLogWriteOnly = new SemaphoreSlim(1);
        private static string LogFileName => $"Log-VacationApp-{DateTime.Now.Date:dd.MM}.txt";
        private static readonly object LOCKING = new object();

        public static void Event(string info)
        {
            TraceLog(LogEventType.Event, info, DateTime.Now);
        }

        public static void Warning(string info)
        {
            TraceLog(LogEventType.Warning, info, DateTime.Now);
        }

        public static void Error(string info)
        {
            TraceLog(LogEventType.Error, info, DateTime.Now);
        }

        private static void TraceLog(LogEventType logEventType, string text, DateTime dateTime)
        {
            OneLogWriteOnly.WaitAsync().GetAwaiter().GetResult();
            Task.Run(() =>
            {
                lock (LOCKING)
                {
                    try
                    {
                        using var isoStore = IsolatedStorageFile.GetUserStoreForApplication();
                        using var writer = new StreamWriter(isoStore.OpenFile(LogFileName, FileMode.Append, FileAccess.Write));
                        writer.WriteLine($"{dateTime.ToShortDateString()} {dateTime:HH:mm:ss.fff}: [{logEventType}] {text}");
                    }
                    finally
                    {
                        OneLogWriteOnly.Release();
                    }
                }
            });
        }
    }

    public enum LogEventType
    {
        Error,
        Warning,
        Event
    }
}