﻿using System.Collections.Generic;
using System.Linq;

namespace System
{
    public static class SystemExtensions
    {
        public static bool IsNullOrEmpty<ITEM>(this IEnumerable<ITEM> collection)
        {
            return collection == null || collection.Count() == 0;
        }
    }
}
