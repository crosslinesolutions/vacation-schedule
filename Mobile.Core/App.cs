﻿using Mobile.Core.ViewModels.LoginPage;
using Mobile.Core.ViewModels.MainPage;
using MvvmCross.ViewModels;

namespace Mobile.Core
{
    public class App : MvxApplication
    {
        public override void Initialize()
        {
            RegisterAppStart<MainPageVM>();
            //LogsServiceOperations.ClearExpiredLogs();
            //CreatableTypes()
            //    .EndingWith("Service")
            //    .AsInterfaces()
            //    .RegisterAsLazySingleton();
            //RegisterCustomAppStart<AppStart>();
            //Log.Event("AppStart registered");
        }
    }
}