﻿using System;

namespace Mobile.Core
{
    [Flags]
    public enum MessageCode
    {
        Ok = 1,
        UndefinedError = 2,
        UndefinedHttpError = 4,
        UndefinedNetworkError = 8,
        Unauthorized = 16,
        BadRequest = 32,
        NotFound = 64,
        InternalServerError = 128,
        ServiceUnavailable = 256,
        GatewayTimeout = 512,
        BadGateway = 1014,
        WaitingForNetwork = 2048,
        DomainNameError = 4096,
        NtpTimeError = 8192,
        HttpTimeout = 16384,
        NetworkUnreachable = 32768,
        ConnectionWasNotEstablished = 65536,
        NoRouteToHost = 131072,
    }

    public enum HttpMethodType
    {
        Get,
        Post
    }
}
