﻿using System.Threading.Tasks;

namespace Mobile.Core.Services
{
    public interface IHttpClientService
    {
        Task<(MessageCode Status, T Data)> HTTP<T>(HttpMethodType type, string url, string request = "");
        Task<(MessageCode, string)> GetToken(string url, string request);
        void SetBaseAddress(string url);
        void ClearRequestHeaders();
        void TryAddHeader(string name, string value);
        void RemoveHeader(string name);
    }
}
