﻿using System;

namespace Mobile.Core.Services
{
    public interface INetworkManager
    {
        event EventHandler<bool> NetworkStatusChanged;
        bool IsNetworkAvailable { get; set; }
    }
}
