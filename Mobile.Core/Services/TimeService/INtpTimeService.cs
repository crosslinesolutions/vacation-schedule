﻿using GeoTimeZone;
using System;
using System.Linq;

namespace Mobile.Core.Services.TimeService
{
    public interface INtpTimeService
    {
        DateTime CurrentTime { get; }
        TimeSpan CurrentTimezone { get; set; }
        DateTime CurrentUTC { get; }

        /// <summary>
        /// Get time zone from latitude and longitude
        /// </summary>
        /// <param name="latitude"></param>
        /// <param name="longitude"></param>
        /// <returns></returns>
        public void DefineTimezone(double latitude, double longitude)
        {
            var currentTimeZoneId = TimeZoneLookup.GetTimeZone(latitude, longitude).Result;
            var tzInfoList = TimeZoneInfo.GetSystemTimeZones();
            var currentTimeZoneOffset = tzInfoList.FirstOrDefault(x => x.Id == currentTimeZoneId).GetUtcOffset(CurrentUTC);
            Log.Event($"[NtpTime] TimeZone='{currentTimeZoneId}', TimezoneOffset='{currentTimeZoneOffset}'");
            CurrentTimezone = currentTimeZoneOffset;
        }
    }
}
