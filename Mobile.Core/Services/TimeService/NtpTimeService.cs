﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace Mobile.Core.Services.TimeService
{
    public abstract class NtpTimeService : INtpTimeService
    {
        private readonly List<string> NTPservers = new List<string>()
        {
            "2.ru.pool.ntp.org",
            "ntp3.stratum2.ru"
        };
        protected DateTime NtpTimeOnStartup = DateTime.MinValue;

        /// <summary>
        /// Returns current NTP time with timeZone included if GPS is enabled, if not then DateTime.Now will be retured
        /// </summary>
        public abstract DateTime CurrentTime { get; }
        public abstract DateTime CurrentUTC { get; }
        public TimeSpan CurrentTimezone { get; set; } = TimeSpan.MinValue;

        protected async Task<MessageCode> TryGetUTC()
        {
            foreach (var ntp in NTPservers)
            {
                try
                {
                    var ntpData = new byte[48];
                    ntpData[0] = 0x1B;
                    var addresses = Dns.GetHostEntry(ntp).AddressList;
                    var ipEndPoint = new IPEndPoint(addresses.First(), 123);
                    Socket socket = new Socket(SocketType.Dgram, ProtocolType.Udp)
                    {
                        ReceiveTimeout = 3000,
                        SendTimeout = 3000
                    };
                    await socket.ConnectAsync(ipEndPoint).ConfigureAwait(false);
                    socket.Send(ntpData);
                    socket.Receive(ntpData);
                    socket.Close();

                    ulong intPart = (ulong)ntpData[40] << 24 | (ulong)ntpData[41] << 16 | (ulong)ntpData[42] << 8 | ntpData[43];
                    ulong fractPart = (ulong)ntpData[44] << 24 | (ulong)ntpData[45] << 16 | (ulong)ntpData[46] << 8 | ntpData[47];

                    var milliseconds = (intPart * 1000) + (fractPart * 1000 / 0x100000000L);
                    NtpTimeOnStartup = new DateTime(1900, 1, 1).AddMilliseconds((long)milliseconds);
                    return MessageCode.Ok;
                }
                catch (Exception e)
                {
                }
            }
            return MessageCode.NtpTimeError;
        }
    }
}
