﻿namespace Mobile.Core.Services
{
    public interface IDialogService
    {
        void ShowMessage(string text);
    }
}
