﻿using Mobile.Core.ViewModels.LoginPage;
using Mobile.Core.ViewModels.MainPage;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using System.Threading.Tasks;

namespace Mobile.Core
{
    public class AppStart : MvxAppStart
    {
        public AppStart(IMvxApplication application, IMvxNavigationService navigationService) : base(application, navigationService)
        {
        }

        protected override void Reset()
        {
            base.Reset();
            //Log.Event("App reset");
        }

        protected override Task<object> ApplicationStartup(object hint = null)
        {
            //Mvx.IoCProvider.TryResolve(out ICachedRepository repository);
            //Mvx.IoCProvider.TryResolve(out IDeviceService device);
            //var safeDeviceModel = device.DeviceModel;
            //Task.Run(() =>
            //{
            //    Log.Event($"AppVersion:'{device.ApplicationVersion} ({device.BuildNumber % 1000})', DeviceModel:'{safeDeviceModel}', DeviceID:'{device.DeviceId}', OsVersion:'{device.OsVersion}'");
            //    if (repository.Department != null)
            //        Log.Event($"Organization:'{repository.Department.Name} ({repository.Department.Id})', HostUrl:'{repository.Settings.ServerUrl}'");
            //});
            return base.ApplicationStartup(hint);
        }

        protected override Task NavigateToFirstViewModel(object hint = null)
        {
            return NavigationService.Navigate<MainPageVM>();
        }
    }
}