﻿using Dto;
using Mobile.Core.Services;
using MvvmCross;
using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using VacationSchedule.Dto;

namespace Mobile.Core.API
{
    public class ApiProvider
    {
        private static ApiProvider instance;
        private readonly IHttpClientService HttpClient;
        private const string BaseServerAddress = "http://172.29.0.100:61353/api/";
        private static readonly object Locking = new object();

        protected ApiProvider()
        {
            HttpClient = Mvx.IoCProvider.Resolve<IHttpClientService>();
            HttpClient.SetBaseAddress(BaseServerAddress);
        }

        public static ApiProvider Instance
        {
            get
            {
                if (instance == null)
                    lock (Locking)
                    {
                        if (instance == null)
                            instance = new ApiProvider();
                    }
                return instance;
            }
        }

        public async Task<MessageCode> Login(UserForRegisterDto request)
        {
            var (status, token) = await HttpClient.GetToken("auth/login", JsonSerializer.Serialize(request)).ConfigureAwait(false);
            if (status != MessageCode.Ok)
                return status;
            var userToken = token.Trim('"');
            HttpClient.ClearRequestHeaders();
            HttpClient.TryAddHeader("Authorization", "Bearer " + userToken);
            return status;
        }

        public async Task<MessageCode> Register(UserForRegisterDto request)
        {
            Log.Event("[ApiProvider] register user");
            var (status, _) = await HttpClient.HTTP<MessageCode>(HttpMethodType.Post, "auth/register", JsonSerializer.Serialize(request)).ConfigureAwait(false);
            if (status != MessageCode.Ok)
                return status;
            status = await Login(request).ConfigureAwait(false);
            return status;
        }

        public async Task<(MessageCode Status, List<CompanyDto> Companies)> GetCompanies()
        {
            Log.Event("[ApiProvider] GetCompanies");
            var (status, companies) = await HttpClient.HTTP<List<CompanyDto>>(HttpMethodType.Get, "companies/usercompanies").ConfigureAwait(false);
            if (status != MessageCode.Ok)
                return (status, null);
            return (status, companies);
        }

        public async Task<MessageCode> CreateCompany(CompanyDto company)
        {
            Log.Event("[ApiProvider] CreateCompany");
            var (status, _) = await HttpClient.HTTP<string>(HttpMethodType.Post, "companies", JsonSerializer.Serialize(company)).ConfigureAwait(false);
            if (status != MessageCode.Ok)
            {
                Log.Warning("[ApiProvider] CreateCompany" + status);
                return (status);
            }
            return (status);
        }

        public async Task<(MessageCode Status, CompanyDto Departments)> GetCompany(int companyId)
        {
            Log.Event("[ApiProvider] GetCompany");
            var (status, company) = await HttpClient.HTTP<CompanyDto>(HttpMethodType.Get, "companies/" + companyId).ConfigureAwait(false);
            if (status != MessageCode.Ok)
                return (status, null);
            return (status, company);
        }

        public async Task<(MessageCode Status, DepartmentDtoResponse Department)> GetDepartment(int departmentId)
        {
            Log.Event("[ApiProvider] GetDepartment");
            var (status, department) = await HttpClient.HTTP<DepartmentDtoResponse>(HttpMethodType.Get, "departments/" + departmentId).ConfigureAwait(false);
            if (status != MessageCode.Ok)
                return (status, null);
            return (status, department);
        }

        public async Task<MessageCode> CreateDepartment(DepartamentDto department)
        {
            Log.Event("[ApiProvider] CreateDepartment");
            var (status, _) = await HttpClient.HTTP<string>(HttpMethodType.Post, "departments", JsonSerializer.Serialize(department)).ConfigureAwait(false);
            if (status != MessageCode.Ok)
            {
                Log.Warning("[ApiProvider] CreateDepartment" + status);
                return (status);
            }
            return (status);
        }

        public async Task<(MessageCode Status, EmployeeDto Employee)> GetEmployee(int emplId)
        {
            Log.Event("[ApiProvider] GetEmployee");
            var (status, employee) = await HttpClient.HTTP<EmployeeDto>(HttpMethodType.Get, "employees/" + emplId).ConfigureAwait(false);
            if (status != MessageCode.Ok)
                return (status, null);
            return (status, employee);
        }

        public async Task<MessageCode> CreateEmployee(EmployeeToCreateDto employee)
        {
            Log.Event("[ApiProvider] CreateEmployee");
            var (status, _) = await HttpClient.HTTP<string>(HttpMethodType.Post, "employees", JsonSerializer.Serialize(employee)).ConfigureAwait(false);
            if (status != MessageCode.Ok)
            {
                Log.Warning("[ApiProvider] CreateEmployee" + status);
                return (status);
            }
            return (status);
        }

        public async Task<MessageCode> CreateVacation(VacationDto vacation)
        {
            Log.Event("[ApiProvider] CreateVacation");
            var (status, _) = await HttpClient.HTTP<string>(HttpMethodType.Post, "vacations", JsonSerializer.Serialize(vacation)).ConfigureAwait(false);
            if (status != MessageCode.Ok)
            {
                Log.Warning("[ApiProvider] CreateVacation" + status);
                return (status);
            }
            return (status);
        }
    }
}
