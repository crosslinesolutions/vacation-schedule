﻿using Mobile.Core.API;
using Mobile.Core.Services;
using Mobile.Core.ViewModels.EmployeePage;
using MvvmCross;
using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;

namespace Mobile.Core.ViewModels.VacationPage
{
    public partial class VacationPageVM : MvxViewModel<VacationItemVM>
    {
        private readonly ApiProvider apiProvider;
        private readonly IDialogService dialogService;
        private readonly IMvxNavigationService navigation;

        private int EmployeeId;

        public VacationPageVM()
        {
            apiProvider = ApiProvider.Instance;
            dialogService = Mvx.IoCProvider.Resolve<IDialogService>();
            navigation = Mvx.IoCProvider.Resolve<IMvxNavigationService>();
        }

        public override void Prepare(VacationItemVM parameter)
        {
            EmployeeId = parameter.EmployeeId;
            BeginDate = parameter.BeginDate;
            EndDate = parameter.EndDate;
            Id = parameter.Id;
        }

        private int _id;
        public int Id
        {
            get => _id;
            set => SetProperty(ref _id, value);
        }

        private string _beginDate;
        public string BeginDate
        {
            get => _beginDate;
            set => SetProperty(ref _beginDate, value);
        }

        private string _endDate;
        public string EndDate
        {
            get => _endDate;
            set => SetProperty(ref _endDate, value);
        }

        private IMvxCommand _createCommand;
        public IMvxCommand CreateCommand => _createCommand ?? (_createCommand = new MvxCommand<MvxViewModel>(CreateVacation));
    }
}
