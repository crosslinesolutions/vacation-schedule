﻿using MvvmCross.ViewModels;
using System;
using System.Threading.Tasks;
using VacationSchedule.Dto;

namespace Mobile.Core.ViewModels.VacationPage
{
    public partial class VacationPageVM
    {
        private async void CreateVacation(MvxViewModel itemVm)
        {
            await Task.Run(async () =>
            {
                var result = await apiProvider.CreateVacation(new VacationDto(Convert.ToDateTime(BeginDate), Convert.ToDateTime(EndDate), 1, EmployeeId)).ConfigureAwait(false);
                if (result != MessageCode.Ok)
                {
                    dialogService.ShowMessage(MessageCodeHandling.GetText(result));
                    Log.Warning("[CreateVacation] vacation was not created");
                }
                else
                {
                    dialogService.ShowMessage("Vacation created");
                }
            });
        }

    }
}
