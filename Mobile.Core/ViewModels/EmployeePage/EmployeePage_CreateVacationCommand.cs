﻿using Mobile.Core.ViewModels.VacationPage;
using System;
using System.Threading.Tasks;
using VacationSchedule.Dto;

namespace Mobile.Core.ViewModels.EmployeePage
{
    public partial class EmployeePageVM
    {
        private Task CreateVacation()
        {
            return Task.Run(() =>
            {
                navigation.Navigate<VacationPageVM, VacationItemVM>(new VacationItemVM(new VacationDto(DateTime.MinValue, DateTime.MinValue, 1, EmployeeId)));
            });
        }
    }
}
