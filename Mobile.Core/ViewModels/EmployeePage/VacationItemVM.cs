﻿using MvvmCross.ViewModels;
using VacationSchedule.Dto;

namespace Mobile.Core.ViewModels.EmployeePage
{
    public class VacationItemVM : MvxViewModel
    {
        public readonly int EmployeeId;

        public VacationItemVM(VacationDto vacation)
        {
            EmployeeId = vacation.EmployeeId;
            Id = vacation.Id;
            BeginDate = vacation.VacationStart.ToShortDateString();
            EndDate = vacation.VacationEnd.ToShortDateString();
        }

        private int _id;
        public int Id
        {
            get => _id;
            set => SetProperty(ref _id, value);
        }

        private string _beginDate;
        public string BeginDate
        {
            get => _beginDate;
            set => SetProperty(ref _beginDate, value);
        }

        private string _endDate;
        public string EndDate
        {
            get => _endDate;
            set => SetProperty(ref _endDate, value);
        }
    }
}
