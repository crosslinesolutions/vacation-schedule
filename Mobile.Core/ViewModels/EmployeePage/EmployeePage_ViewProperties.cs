﻿using Mobile.Core.API;
using Mobile.Core.Services;
using Mobile.Core.ViewModels.DepartmentPage;
using MvvmCross;
using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;

namespace Mobile.Core.ViewModels.EmployeePage
{
    public partial class EmployeePageVM : MvxViewModel<EmplItemVm>
    {
        private readonly ApiProvider apiProvider;
        private readonly IDialogService dialogService;
        private readonly IMvxNavigationService navigation;

        private int EmployeeId;

        public EmployeePageVM()
        {
            apiProvider = ApiProvider.Instance;
            dialogService = Mvx.IoCProvider.Resolve<IDialogService>();
            navigation = Mvx.IoCProvider.Resolve<IMvxNavigationService>();
        }

        public override void Prepare(EmplItemVm parameter) => EmployeeId = parameter.Id;

        public override void ViewAppearing()
        {
            base.ViewAppearing();
            LoadEmployee();
        }

        private MvxObservableCollection<MvxViewModel> _vacation;
        public MvxObservableCollection<MvxViewModel> Vacations
        {
            get => _vacation;
            set => SetProperty(ref _vacation, value);
        }

        private string _firstName;
        public string FirstName
        {
            get => _firstName;
            set => SetProperty(ref _firstName, value);
        }

        private string _secondName;
        public string SecondName
        {
            get => _secondName;
            set => SetProperty(ref _secondName, value);
        }

        private string _position;
        public string Position
        {
            get => _position;
            set => SetProperty(ref _position, value);
        }

        private IMvxCommand _openVacationCommand;
        public IMvxCommand OpenVacationCommand => _openVacationCommand ?? (_openVacationCommand = new MvxCommand<MvxViewModel>(OpenVacation));

        private IMvxAsyncCommand _createVacationCommand;
        public IMvxAsyncCommand CreateVacationCommand => _createVacationCommand ?? (_createVacationCommand = new MvxAsyncCommand(CreateVacation));
    }
}
