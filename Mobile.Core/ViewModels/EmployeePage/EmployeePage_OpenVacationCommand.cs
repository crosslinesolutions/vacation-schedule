﻿using Mobile.Core.ViewModels.VacationPage;
using MvvmCross.ViewModels;
using System.Threading.Tasks;

namespace Mobile.Core.ViewModels.EmployeePage
{
    public partial class EmployeePageVM
    {
        private async void OpenVacation(MvxViewModel itemVm)
        {
            await Task.Run(() =>
            {
                navigation.Navigate<VacationPageVM, VacationItemVM>(itemVm as VacationItemVM);
            });
        }
    }
}
