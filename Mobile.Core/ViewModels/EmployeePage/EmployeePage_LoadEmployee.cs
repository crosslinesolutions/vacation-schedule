﻿using MvvmCross.ViewModels;
using System.Collections.Generic;
using System.Linq;

namespace Mobile.Core.ViewModels.EmployeePage
{
    public partial class EmployeePageVM
    {
        private async void LoadEmployee()
        {
            var (status, employee) = await apiProvider.GetEmployee(EmployeeId).ConfigureAwait(false);
            if (status != MessageCode.Ok)
            {
                dialogService.ShowMessage(MessageCodeHandling.GetText(status));
                return;
            }
            List<VacationItemVM> vacationVM = new List<VacationItemVM>();
            var query = from vacation in employee.Vacations
                        orderby vacation.Id
                        select vacation;
            foreach (var vacation in query)
                vacationVM.Add(new VacationItemVM(vacation));
            Vacations = new MvxObservableCollection<MvxViewModel>(vacationVM);
            Position = employee.Position;
            FirstName = employee.FirstName;
            SecondName = employee.LastName;
        }
    }
}
