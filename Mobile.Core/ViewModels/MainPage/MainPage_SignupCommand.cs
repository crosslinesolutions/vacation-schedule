﻿using Mobile.Core.ViewModels.LoginPage;
using System.Threading.Tasks;

namespace Mobile.Core.ViewModels.MainPage
{
    public partial class MainPageVM
    {
        private async Task Signup()
        {
            await Task.Run(async () =>
            {
                await navigation.Navigate<LoginPageVM, bool>(true);
            });
        }

    }
}
