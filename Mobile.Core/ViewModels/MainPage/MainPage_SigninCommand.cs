﻿using Mobile.Core.ViewModels.LoginPage;
using System.Threading.Tasks;

namespace Mobile.Core.ViewModels.MainPage
{
    public partial class MainPageVM
    {
        private async Task Signin()
        {
            await Task.Run(async () =>
            {
                await navigation.Navigate<LoginPageVM>();
            });
        }

    }
}
