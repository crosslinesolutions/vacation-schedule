﻿using MvvmCross;
using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;

namespace Mobile.Core.ViewModels.MainPage
{
    public partial class MainPageVM : MvxViewModel
    {
        private readonly IMvxNavigationService navigation;

        public MainPageVM()
        {
            navigation = Mvx.IoCProvider.Resolve<IMvxNavigationService>();
        }


        private IMvxAsyncCommand _signinCommand;
        public IMvxAsyncCommand SigninCommand => _signinCommand ?? (_signinCommand = new MvxAsyncCommand(Signin));


        private IMvxAsyncCommand _signupCommand;
        public IMvxAsyncCommand SignupCommand => _signupCommand ?? (_signupCommand = new MvxAsyncCommand(Signup));
    }
}
