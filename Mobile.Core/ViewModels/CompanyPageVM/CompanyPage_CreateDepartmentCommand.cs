﻿using MvvmCross.ViewModels;
using System;
using System.Threading.Tasks;
using VacationSchedule.Dto;

namespace Mobile.Core.ViewModels.CompanyPage
{
    public partial class CompanyPageVM
    {
        private async void CreateDepartment(MvxViewModel itemVm)
        {
            await Task.Run(async () =>
            {
                if (IsCreateFieldsVisible is false)
                    IsCreateFieldsVisible = true;
                else
                {
                    var result = await apiProvider.CreateDepartment(new DepartamentDto(NewDepartmentName, CompanyId, "NotProvided")).ConfigureAwait(false);
                    if (result != MessageCode.Ok)
                    {
                        dialogService.ShowMessage(MessageCodeHandling.GetText(result));
                        Log.Warning("[CreateCompany] company was not created");
                    }
                    else
                    {
                        IsCreateFieldsVisible = false;
                        dialogService.ShowMessage("Company created");
                        LoadCompany();
                    }
                }
            });
        }
    }
}
