﻿using Mobile.Core.ViewModels.DepartmentPage;
using MvvmCross.ViewModels;
using System.Threading.Tasks;

namespace Mobile.Core.ViewModels.CompanyPage
{
    public partial class CompanyPageVM
    {
        private async void OpenDepartment(MvxViewModel itemVm)
        {
            await Task.Run(() =>
            {
                navigation.Navigate<DepartmentPageVM, DepartmentItemVM>(itemVm as DepartmentItemVM);
            });
        }
    }
}