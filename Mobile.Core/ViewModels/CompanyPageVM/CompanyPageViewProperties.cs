﻿using Mobile.Core.API;
using Mobile.Core.Services;
using Mobile.Core.ViewModels.AccountPage;
using MvvmCross;
using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace Mobile.Core.ViewModels.CompanyPage
{
    public partial class CompanyPageVM : MvxViewModel<OrganizationItemVM>
    {
        private readonly ApiProvider apiProvider;
        private readonly IDialogService dialogService;
        private readonly IMvxNavigationService navigation;
        private int CompanyId;

        public CompanyPageVM()
        {
            apiProvider = ApiProvider.Instance;
            dialogService = Mvx.IoCProvider.Resolve<IDialogService>();
            navigation = Mvx.IoCProvider.Resolve<IMvxNavigationService>();
        }

        public override void Prepare(OrganizationItemVM parameter) => CompanyId = parameter.Id;

        public override Task Initialize()
        {
            LoadCompany();
            return base.Initialize();
        }

        private MvxObservableCollection<MvxViewModel> _departments;
        public MvxObservableCollection<MvxViewModel> Departments
        {
            get => _departments;
            set => SetProperty(ref _departments, value);
        }

        private async void LoadCompany()
        {
            var (status, company) = await apiProvider.GetCompany(CompanyId).ConfigureAwait(false);
            if (status != MessageCode.Ok)
            {
                dialogService.ShowMessage(MessageCodeHandling.GetText(status));
                return;
            }
            List<DepartmentItemVM> departmentsVM = new List<DepartmentItemVM>();
            var query = from department in company.Departments
                        orderby department.Id
                        select department;
            foreach (var department in query)
                departmentsVM.Add(new DepartmentItemVM(department));
            Departments = new MvxObservableCollection<MvxViewModel>(departmentsVM);
        }

        private IMvxCommand _openDepartmentCommand;
        public IMvxCommand OpenDepartmentCommand => _openDepartmentCommand ?? (_openDepartmentCommand = new MvxCommand<MvxViewModel>(OpenDepartment));

        private IMvxCommand _createDepartmentCommand;
        public IMvxCommand CreateDepartmentCommand => _createDepartmentCommand ?? (_createDepartmentCommand = new MvxCommand<MvxViewModel>(CreateDepartment));

        private string _newDepartmentName;
        public string NewDepartmentName
        {
            get => _newDepartmentName;
            set => SetProperty(ref _newDepartmentName, value);
        }

        private bool _isCreateFieldsVisible = false;
        public bool IsCreateFieldsVisible
        {
            get => _isCreateFieldsVisible;
            set => SetProperty(ref _isCreateFieldsVisible, value);
        }
    }
}
