﻿using MvvmCross.ViewModels;
using VacationSchedule.Dto;

namespace Mobile.Core.ViewModels.CompanyPage
{
    public class DepartmentItemVM : MvxViewModel
    {
        public DepartmentItemVM(DepartamentDto department)
        {
            Name = department.Name;
            Id = department.Id;
            Info = department.Description;
        }

        private string _name;
        public string Name
        {
            get => _name;
            set => SetProperty(ref _name, value);
        }

        private int _id;
        public int Id
        {
            get => _id;
            set => SetProperty(ref _id, value);
        }

        private string _info;
        public string Info
        {
            get => _info;
            set => SetProperty(ref _info, value);
        }
    }
}
