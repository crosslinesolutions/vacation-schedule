﻿using Mobile.Core.ViewModels.EmployeePage;
using MvvmCross.ViewModels;
using System.Threading.Tasks;

namespace Mobile.Core.ViewModels.DepartmentPage
{
    public partial class DepartmentPageVM
    {
        private async void OpenEmpl(MvxViewModel itemVm)
        {
            await Task.Run(() =>
            {
                navigation.Navigate<EmployeePageVM, EmplItemVm>(itemVm as EmplItemVm);
            });
        }
    }
}
