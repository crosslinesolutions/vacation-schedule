﻿using MvvmCross.ViewModels;
using System.Collections.Generic;
using System.Linq;

namespace Mobile.Core.ViewModels.DepartmentPage
{
    public partial class DepartmentPageVM
    {
        private async void LoadDepartment()
        {
            var (status, department) = await apiProvider.GetDepartment(DepartmentId).ConfigureAwait(false);
            if (status != MessageCode.Ok)
            {
                dialogService.ShowMessage(MessageCodeHandling.GetText(status));
                return;
            }
            List<EmplItemVm> emplVM = new List<EmplItemVm>();
            foreach (var empl in department.Employees.OrderBy(empl=>empl.Id))
                emplVM.Add(new EmplItemVm(empl));
            Employees = new MvxObservableCollection<MvxViewModel>(emplVM);
        }
    }
}
