﻿using MvvmCross.ViewModels;
using System;
using System.Threading.Tasks;
using VacationSchedule.Dto;

namespace Mobile.Core.ViewModels.DepartmentPage
{
    public partial class DepartmentPageVM
    {
        private async void CreateEmployee(MvxViewModel itemVm)
        {
            await Task.Run(async () =>
            {
                if (IsCreateFieldsVisible is false)
                    IsCreateFieldsVisible = true;
                else
                {
                    var result = await apiProvider.CreateEmployee(new EmployeeToCreateDto(NewEmployeeFirstName, NewEmployeeSecondName, "programmer", DepartmentId)).ConfigureAwait(false);
                    if (result != MessageCode.Ok)
                    {
                        dialogService.ShowMessage(MessageCodeHandling.GetText(result));
                        Log.Warning("[CreateEmployee] employee was not created");
                    }
                    else
                    {
                        IsCreateFieldsVisible = false;
                        dialogService.ShowMessage("Employee created");
                        LoadDepartment();
                    }
                }
            });
        }
    }
}
