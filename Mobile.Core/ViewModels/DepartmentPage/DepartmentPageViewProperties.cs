﻿using Mobile.Core.API;
using Mobile.Core.Services;
using Mobile.Core.ViewModels.CompanyPage;
using MvvmCross;
using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Mobile.Core.ViewModels.DepartmentPage
{
    public partial class DepartmentPageVM : MvxViewModel<DepartmentItemVM>
    {
        private readonly ApiProvider apiProvider;
        private readonly IDialogService dialogService;
        private readonly IMvxNavigationService navigation;

        private int DepartmentId;

        public DepartmentPageVM()
        {
            apiProvider = ApiProvider.Instance;
            dialogService = Mvx.IoCProvider.Resolve<IDialogService>();
            navigation = Mvx.IoCProvider.Resolve<IMvxNavigationService>();
        }

        public override void Prepare(DepartmentItemVM parameter) => DepartmentId = parameter.Id;

        public override Task Initialize()
        {
            LoadDepartment();
            return base.Initialize();
        }

        private MvxObservableCollection<MvxViewModel> _employees;
        public MvxObservableCollection<MvxViewModel> Employees
        {
            get => _employees;
            set => SetProperty(ref _employees, value);
        }

        private IMvxCommand _openEmplCommand;
        public IMvxCommand OpenEmplCommand => _openEmplCommand ?? (_openEmplCommand = new MvxCommand<MvxViewModel>(OpenEmpl));

        private IMvxCommand _createEmployeeCommand;
        public IMvxCommand CreateEmployeeCommand => _createEmployeeCommand ?? (_createEmployeeCommand = new MvxCommand<MvxViewModel>(CreateEmployee));

        private string _newEmployeeFirstName;
        public string NewEmployeeFirstName
        {
            get => _newEmployeeFirstName;
            set => SetProperty(ref _newEmployeeFirstName, value);
        }

        private string _newEmployeeSecondName;
        public string NewEmployeeSecondName
        {
            get => _newEmployeeSecondName;
            set => SetProperty(ref _newEmployeeSecondName, value);
        }

        private bool _isCreateFieldsVisible = false;
        public bool IsCreateFieldsVisible
        {
            get => _isCreateFieldsVisible;
            set => SetProperty(ref _isCreateFieldsVisible, value);
        }
    }
}
