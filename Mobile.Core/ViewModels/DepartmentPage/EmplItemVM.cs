﻿using MvvmCross.ViewModels;
using VacationSchedule.Dto;

namespace Mobile.Core.ViewModels.DepartmentPage
{
    public class EmplItemVm : MvxViewModel
    {
        public EmplItemVm(EmployeeDto empl)
        {
            Name = empl.FirstName;
            SecondName = empl.LastName;
            Id = empl.Id;
            Position = empl.Position;
        }

        private string _name;
        public string Name
        {
            get => _name;
            set => SetProperty(ref _name, value);
        }

        private string _secondName;
        public string SecondName
        {
            get => _secondName;
            set => SetProperty(ref _secondName, value);
        }

        private int _id;
        public int Id
        {
            get => _id;
            set => SetProperty(ref _id, value);
        }

        private string _position;
        public string Position
        {
            get => _position;
            set => SetProperty(ref _position, value);
        }
    }
}
