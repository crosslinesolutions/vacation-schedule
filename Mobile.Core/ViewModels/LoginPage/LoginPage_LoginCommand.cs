﻿using Mobile.Core.ViewModels.AccountPage;
using System.Threading.Tasks;
using VacationSchedule.Dto;

namespace Mobile.Core.ViewModels.LoginPage
{
    public partial class LoginPageVM
    {
        private async Task Login()
        {
            await Task.Run(async () =>
            {
                if (networkManager.IsNetworkAvailable is false)
                {
                    dialogService.ShowMessage("Сеть недоступна. Проверьте подключение к интернету.");
                    return;
                }
                var status = await apiProvider.Login(new UserForRegisterDto(LoginField, PasswordField)).ConfigureAwait(false);
                if (status != MessageCode.Ok)
                    dialogService.ShowMessage(MessageCodeHandling.GetText(status));
                else
                {
                    dialogService.ShowMessage("Вы усешно авторизованы");
                    await Task.Delay(2000);
                    _ = navigation.Navigate<AccountPageVM>();
                }
            });
        }
    }
}
