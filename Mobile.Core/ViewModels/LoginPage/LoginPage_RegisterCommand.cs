﻿using Mobile.Core.ViewModels.AccountPage;
using System.Threading.Tasks;
using VacationSchedule.Dto;

namespace Mobile.Core.ViewModels.LoginPage
{
    public partial class LoginPageVM
    {
        private async Task Register()
        {
            await Task.Run(async () =>
            {
                var status = await apiProvider.Register(new UserForRegisterDto(
                    LoginField, PasswordField)).ConfigureAwait(false);
                if (status != MessageCode.Ok)
                    dialogService.ShowMessage(MessageCodeHandling.GetText(status));
                else
                {
                    dialogService.ShowMessage("Вы успешно зарегистрированы");
                    await Task.Delay(2000);
                    _ = navigation.Navigate<AccountPageVM>();
                }
            });
        }
    }
}
