﻿using Mobile.Core.API;
using Mobile.Core.Services;
using Mobile.Core.Services.TimeService;
using MvvmCross;
using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using System;
using System.Threading.Tasks;

namespace Mobile.Core.ViewModels.LoginPage
{
    public partial class LoginPageVM : MvxViewModel<bool>
    {
        private readonly ApiProvider apiProvider;
        private readonly IDialogService dialogService;
        private readonly IMvxNavigationService navigation;
        private readonly INetworkManager networkManager;
        private readonly INtpTimeService timeService;
        private bool IsSignup = false;

        public LoginPageVM()
        {
            apiProvider = ApiProvider.Instance;
            dialogService = Mvx.IoCProvider.Resolve<IDialogService>();
            navigation = Mvx.IoCProvider.Resolve<IMvxNavigationService>();
            networkManager = Mvx.IoCProvider.Resolve<INetworkManager>();
            timeService = Mvx.IoCProvider.Resolve<INtpTimeService>();
        }

        public override void Prepare(bool isSignup)
        {
            if (isSignup)
            {
                PasswordField2Visibility = true;
                HeaderText = "Регистрация";
            }
        }

        public override Task Initialize()
        {
            var fakeLat = 59.221322661;
            var fakeLong = 52.5122353215;
            timeService.DefineTimezone(fakeLat, fakeLong);
            return base.Initialize();
        }

        public override void ViewAppeared()
        {
            base.ViewAppeared();
            Log.Event($"[LoginPage] [ViewAppeared] current time: {timeService.CurrentTime}");
        }

        private string _loginField;
        public string LoginField
        {
            get => _loginField;
            set => SetProperty(ref _loginField, value);
        }

        private string _passwordField;
        public string PasswordField
        {
            get => _passwordField;
            set => SetProperty(ref _passwordField, value);
        }

        private string _headerText;
        public string HeaderText
        {
            get => _headerText;
            set => SetProperty(ref _headerText, value);
        }

        private string _passwordField2;
        public string PasswordField2
        {
            get => _passwordField2;
            set => SetProperty(ref _passwordField2, value);
        }

        private bool _passwordField2Visibility;
        public bool PasswordField2Visibility
        {
            get => _passwordField2Visibility;
            set
            {
                SetProperty(ref _passwordField2Visibility, value);
                IsSignup = true;
            }
        }

        private IMvxAsyncCommand _loginCommand;
        public IMvxAsyncCommand LoginCommand => _loginCommand ?? (_loginCommand = IsSignup ? new MvxAsyncCommand(Register) : new MvxAsyncCommand(Login));

        private bool _loginButtonEnabled;
        public bool LoginButtonEnabled
        {
            get => _loginButtonEnabled;
            set => SetProperty(ref _loginButtonEnabled, value);
        }
    }
}
