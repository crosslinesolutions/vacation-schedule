﻿using Mobile.Core.ViewModels.CompanyPage;
using MvvmCross.ViewModels;
using System.Threading.Tasks;

namespace Mobile.Core.ViewModels.AccountPage
{
    public partial class AccountPageVM
    {
        private async void OpenCompany(MvxViewModel itemVm)
        {
            await Task.Run(() =>
            {
                navigation.Navigate<CompanyPageVM, OrganizationItemVM>(itemVm as OrganizationItemVM);
            });
        }
    }
}
