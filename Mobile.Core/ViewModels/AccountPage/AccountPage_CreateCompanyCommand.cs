﻿using MvvmCross.ViewModels;
using System;
using System.Threading.Tasks;
using VacationSchedule.Dto;

namespace Mobile.Core.ViewModels.AccountPage
{
    public partial class AccountPageVM
    {
        private async void CreateCompany(MvxViewModel itemVm)
        {
            await Task.Run(async () =>
            {
                if (IsCreateFieldsVisible is false)
                    IsCreateFieldsVisible = true;
                else
                {
                    var result = await apiProvider.CreateCompany(new CompanyDto() { Name = NewCompanyName, UserId = UserId }).ConfigureAwait(false);
                    if (result != MessageCode.Ok)
                    {
                        dialogService.ShowMessage(MessageCodeHandling.GetText(result));
                        Log.Warning("[CreateCompany] company was not created");
                    }
                    else
                    {
                        IsCreateFieldsVisible = false;
                        dialogService.ShowMessage("Company created");
                        LoadCompanies();
                    }
                }
            });
        }
    }
}
