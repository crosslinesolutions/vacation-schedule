﻿using Mobile.Core.API;
using Mobile.Core.Services;
using MvvmCross;
using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mobile.Core.ViewModels.AccountPage
{
    public partial class AccountPageVM : MvxViewModel
    {
        private readonly ApiProvider apiProvider;
        private readonly IDialogService dialogService;
        private readonly IMvxNavigationService navigation;
        private readonly INetworkManager networkManager;


        private int UserId;

        public AccountPageVM()
        {
            apiProvider = ApiProvider.Instance;
            dialogService = Mvx.IoCProvider.Resolve<IDialogService>();
            navigation = Mvx.IoCProvider.Resolve<IMvxNavigationService>();
            networkManager = Mvx.IoCProvider.Resolve<INetworkManager>();
        }

        private MvxObservableCollection<MvxViewModel> _organizations;
        public MvxObservableCollection<MvxViewModel> Companies
        {
            get => _organizations;
            set => SetProperty(ref _organizations, value);
        }

        private IMvxCommand _openDepartmentCommand;
        public IMvxCommand OpenDepartmentCommand => _openDepartmentCommand ?? (_openDepartmentCommand = new MvxCommand<MvxViewModel>(OpenCompany));

        private IMvxCommand _createCompanyCommand;
        public IMvxCommand CreateCompanyCommand => _createCompanyCommand ?? (_createCompanyCommand = new MvxCommand<MvxViewModel>(CreateCompany));

        private string _newCompanyName;
        public string NewCompanyName
        {
            get => _newCompanyName;
            set => SetProperty(ref _newCompanyName, value);
        }

        private bool _isCreateFieldsVisible = false;
        public bool IsCreateFieldsVisible
        {
            get => _isCreateFieldsVisible;
            set => SetProperty(ref _isCreateFieldsVisible, value);
        }

        private bool _isInternetStatusVisible = false;
        public bool IsInternetStatusVisible
        {
            get => _isInternetStatusVisible;
            set => SetProperty(ref _isInternetStatusVisible, value);
        }

        private string _internetStatus;
        public string InternetStatus
        {
            get => _internetStatus;
            set => SetProperty(ref _internetStatus, value);
        }

        public override Task Initialize()
        {
            LoadCompanies();
            networkManager.NetworkStatusChanged += NetworkStatusChanged;
            return base.Initialize();
        }

        private void NetworkStatusChanged(object sender, bool isInternetAvailable)
        {
            if (isInternetAvailable && IsInternetStatusVisible)
                IsInternetStatusVisible = false;
            if (isInternetAvailable is false)
            {
                IsInternetStatusVisible = true;
                InternetStatus = "Ожидание сети...";
            }
        }

        private async void LoadCompanies()
        {
            var (status, companies) = await apiProvider.GetCompanies();
            if (status != MessageCode.Ok)
            {
                dialogService.ShowMessage(MessageCodeHandling.GetText(status));
                return;
            }
            List<OrganizationItemVM> organizationsVM = new List<OrganizationItemVM>();
            foreach (var company in companies)
                organizationsVM.Add(new OrganizationItemVM(company));
            Companies = new MvxObservableCollection<MvxViewModel>(organizationsVM);
            if (companies.Count > 0)
                UserId = companies.FirstOrDefault().UserId;
        }
    }
}
