﻿using MvvmCross.ViewModels;
using VacationSchedule.Dto;

namespace Mobile.Core.ViewModels.AccountPage
{
    public class OrganizationItemVM : MvxViewModel
    {
        public OrganizationItemVM(CompanyDto company)
        {
            Name = company.Name;
            Id = company.Id;
        }

        private string _name;
        public string Name
        {
            get => _name;
            set => SetProperty(ref _name, value);
        }

        private int _id;
        public int Id
        {
            get => _id;
            set => SetProperty(ref _id, value);
        }
    }
}
