﻿using System;
using System.Collections.Generic;
using System.Text;
using Core.Entities;
using Core.Interfaces;
using VacationSchedule.Core.Entities;

namespace VacationSchedule.WebApi.ReadFile
{
    public class ReadFileContext
    {
        public IStrategyReadFile ContextStrategy { get; set; }
        public string Path { get; set; }
        public int DepartamentId { get; set; }
  

        public ReadFileContext(IStrategyReadFile strategy)
        {
            ContextStrategy = strategy;
        }

        public List<Employee> Execute()
        {
           return ContextStrategy.Read(Path, DepartamentId);
        }
    }
}
