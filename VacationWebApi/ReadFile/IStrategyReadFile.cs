﻿using System;
using System.Collections.Generic;
using System.Text;
using Core.Entities;
using VacationSchedule.Core.Entities;

namespace VacationSchedule.WebApi.ReadFile
{
    public interface IStrategyReadFile
    {
        List<Employee> Read(string path, int departamentId);
    }
}
