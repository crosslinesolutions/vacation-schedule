﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Core.Entities;
using VacationSchedule.WebApi.CustomValidation;

namespace VacationSchedule.WebApi.Interfaces
{
    public interface IVacationService
    {
        Task CreateVacation(VacationDto vacation);
        Task UpdateVacation(VacationDto vacation, int vacationId);

        Task<Dictionary<string, string>> VaidateCrossDays(VacationDto dto);
    }
}
