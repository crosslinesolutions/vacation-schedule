﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace VacationSchedule.WebApi.Interfaces
{
    public interface IFileService
    {
        Task<string> SaveFile(IFormFile file);
        Task<Dictionary<string, string>> ValidateFile(IFormFile file);

        void DeleteFile(string path);

    }
}
