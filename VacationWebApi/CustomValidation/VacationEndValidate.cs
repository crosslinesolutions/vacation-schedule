﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace VacationSchedule.WebApi.CustomValidation
{
    public class VacationEndValidate : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {

            var model = (VacationDto)validationContext.ObjectInstance;

            DateTime start = model.VacationStart;
            DateTime end = model.VacationEnd;

            TimeSpan difference = end - start;

            if (difference.Days < 0)
                return new ValidationResult("vacation end date not valid");

            return ValidationResult.Success;

        }
    }
}
