﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Dto;
using VacationSchedule.Dto;

namespace VacationSchedule.WebApi.CustomValidation
{
    public class VacationDto  
    {
        public int Id { get; set; }
        [Required]
        public int EmployeeId { get; set; }
        public EmployeeDto Employee { get; set; }
        [Required]
        public int VacatyionTypeId { get; set; }
        public VacatyionTypeDto VacationType { get; set; }

        [VacationStratValidate]
        [Required]
        public DateTime VacationStart { get ; set; }
        [Required]
        [VacationEndValidate]
        public DateTime VacationEnd { get; set; }
        public List<VacationDayDto> VacationDays { get; set; }
        public int TotalVacationDays { get; set; }
        public VacationStatus Status { get; set; }
        public enum VacationStatus
        {
            Creation,
            OnMatching,
            Agreed,
            Cancel,
            Close
        }
    }
}
