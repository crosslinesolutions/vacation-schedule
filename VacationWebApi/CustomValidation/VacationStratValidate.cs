﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace VacationSchedule.WebApi.CustomValidation
{
    public class VacationStratValidate : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {

            var model = (VacationDto)validationContext.ObjectInstance;

            DateTime start = model.VacationStart;
            DateTime end = model.VacationEnd;

            DateTime today = DateTime.Today;

            if (start <= today)
            {
                return new ValidationResult("Start date not valid");
            }

            return ValidationResult.Success;

        }
    }
}
