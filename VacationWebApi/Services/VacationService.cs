﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using VacationSchedule.Core.Entities;
using VacationSchedule.Core.Interfaces;
using VacationSchedule.WebApi.CustomValidation;
using VacationSchedule.WebApi.Interfaces;

namespace VacationSchedule.WebApi.Services
{
    public class VacationService
       : IVacationService
    {
        private readonly IAsyncRepository<Vacation> _asyncGeneticRepositoryVacation;
        private readonly IAsyncRepository<VacationDay> _asyncGenericRepositoryDays;
        private readonly IVacationRepository _vacationRepository;
        private readonly IEmployeesRepository _employeesRepository;

        public VacationService(IAsyncRepository<Vacation> asyncRepositoryVacation, IAsyncRepository<VacationDay> asyncRepositoryDays, IVacationRepository vacationRepository, IEmployeesRepository employeesRepository)
        {
            _asyncGeneticRepositoryVacation = asyncRepositoryVacation;
            _asyncGenericRepositoryDays = asyncRepositoryDays;
            _vacationRepository = vacationRepository;
            _employeesRepository = employeesRepository;
        }

        public async Task CreateVacation(VacationDto vacationDto)
        {
            Vacation vacationToCreate = new Vacation
            {
                EmployeeId = vacationDto.EmployeeId,
                VacatyionTypeId = vacationDto.VacatyionTypeId,
                VacationStart = vacationDto.VacationStart,
                VacationEnd = vacationDto.VacationEnd
            };

            var daysList = await GetDifferenceVacationDays(vacationDto, vacationToCreate);
            vacationToCreate.VacationDays = daysList;
            await _asyncGeneticRepositoryVacation.AddAsync(vacationToCreate);
        }

        public async Task UpdateVacation(VacationDto vacationDto, int vacationId)
        {
            Vacation vacationFromDb = await _vacationRepository.GetVacationById(vacationId);

            vacationFromDb.VacatyionTypeId = vacationDto.VacatyionTypeId;
            vacationFromDb.VacationStart = vacationDto.VacationStart;
            vacationFromDb.VacationEnd = vacationDto.VacationEnd;
            //очистить дни отпуска
            await _asyncGenericRepositoryDays.DeleteRangeAsync(vacationFromDb.VacationDays);

            //записать заново дни отпуска
            var daysList = await GetDifferenceVacationDays(vacationDto, vacationFromDb);

            vacationFromDb.VacationDays = daysList;
            await _asyncGeneticRepositoryVacation.UpdateAsync(vacationFromDb);
        }

        public async Task<Dictionary<string, string>> VaidateCrossDays(VacationDto dto)
        {
            Dictionary<string, string> errors = new Dictionary<string, string>();

            List<DateTime> daysInNewVacation = DatesInNewVacation(dto);

            var employee = await _employeesRepository.GetEmployeeById(dto.EmployeeId);

            var vacationsInDepartament = await _vacationRepository.GetAllVacationInDepartament(employee.DepartamentId);

            List<VacationDay> crossDays = new List<VacationDay>();

            foreach (var day in vacationsInDepartament.SelectMany(d => d.VacationDays))
            {
                //Проверяем содержит ли уже созданный отпуск дату из создаваемого диапазона
                if (daysInNewVacation.Contains(day.Dt))
                {
                    //Если да, добавляем в коллекцию персекающихся дней.
                    crossDays.Add(day);
                }
            }

            //Группируем дни отпуска по отпускам принадлежащим сотрудникам.
            var res = crossDays.GroupBy(e => e.Vacation);
            var count = res.Count();
            
            //Если количество персекающихся отпусков равно или больше разрешонного правилом департамента - возвращяем ошибку
            if (count >= employee.Departament.SameTimePeopleOnVacation)
            {
                errors.Add("SameTimePeopleOnVacation", "violated the rule of maximum people on vacation");
            }

            return errors;
        }

        private async Task<List<VacationDay>> GetDifferenceVacationDays(VacationDto vacationDto,
            Vacation vacationToCreate)
        {

            int difference = vacationDto.VacationEnd.Subtract(vacationDto.VacationStart).Days;

            List<VacationDay> daysList = new List<VacationDay>();
            IsDayoffService dayoffService = new IsDayoffService(new HttpClient());

            for (var i = 0; i <= difference; i++)
            {
                daysList.Add(new VacationDay()
                {
                    Dt = vacationDto.VacationStart.AddDays(i),
                    Vacation = vacationToCreate,
                    IsWorkDay = await dayoffService.GetDayInfo(vacationDto.VacationStart.AddDays(i)), //проверка рабочий день или нет
                });
            }

            return daysList;
        }


        private static List<DateTime> DatesInNewVacation(VacationDto dto)
        {
            int difference = dto.VacationEnd.Subtract(dto.VacationStart).Days;

            var daysInNewVacation = new List<DateTime>();

            for (var i = 0; i <= difference; i++)
            {
                daysInNewVacation.Add(dto.VacationStart.AddDays(i));
            }

            return daysInNewVacation;
        }

    }
}
