﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using VacationSchedule.WebApi.Interfaces;

namespace VacationSchedule.WebApi.Services
{
    public class FileService : IFileService
    {
        public async Task<string> SaveFile(IFormFile file)
        {

            var folderName = Path.Combine("Resources", "uploads");
            var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);

            string fileName = Guid.NewGuid().ToString() + Path.GetExtension(file.FileName); ;

            // var fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');

            var fullPath = Path.Combine(pathToSave, fileName);
            var dbPath = Path.Combine(folderName, fileName);

            using (var stream = new FileStream(fullPath, FileMode.Create))
            {
                file.CopyTo(stream);
            }

            return dbPath;
        }

        public async Task<Dictionary<string, string>> ValidateFile(IFormFile file)
        {
            string[] _extensions = new[]
            {
                ".xlsx", ".csv", ".xls"
            };
            Dictionary<string, string> errors = new Dictionary<string, string>();

            var extension = Path.GetExtension(file.FileName);

            if (file.Length < 1)
            {
                errors.Add("file", "error upload");
            }

            if(!_extensions.Contains(extension.ToLower()))
            {
                errors.Add("file", "Unsupported file type. Only excel or csv");
            }
            return errors;

        }

        public void DeleteFile(string path)
        {
            if (File.Exists(path))
            {
                File.Delete(path);
            }
        }
    }
}
