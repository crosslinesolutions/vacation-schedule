﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace VacationSchedule.WebApi.Services
{
    public class IsDayoffService
    {
        public HttpClient Client { get; }

        public IsDayoffService(HttpClient client)
        {
            client.BaseAddress = new Uri("https://isdayoff.ru");
            Client = client;
        }

        public async Task<bool> GetDayInfo(DateTime date)
        {
            var response = await Client.GetAsync(date.ToString("yyyyMMdd"));
            var responseStream = await response.Content.ReadAsStringAsync();
           
            switch (responseStream)
            {
                case "0":
                    return true;
                    break;
                default:
                    return false;
                    break;
            }

        }
    }
}
