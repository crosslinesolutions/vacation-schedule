﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using VacationSchedule.Core.Entities;
using VacationSchedule.Core.Interfaces;
using VacationSchedule.Dto;

namespace VacationSchedule.WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class CompaniesController : ControllerBase
    {
        private IAsyncRepository<Company> _asyncRepositoryCompanyT;
        private ICompanyRepository _asyncRepositoryCompany;
        public CompaniesController(IAsyncRepository<Company> asyncRepositoryCompanyT, ICompanyRepository asyncRepositoryCompany)
        {
            _asyncRepositoryCompanyT = asyncRepositoryCompanyT;
            _asyncRepositoryCompany = asyncRepositoryCompany;
        }

        // GET: api/Companies
        [HttpGet("usercompanies")]
        public async Task<IReadOnlyList<Company>> Getcompanies()
        {
            string userId = User.Claims
                .FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier)
                ?.Value;

            return await _asyncRepositoryCompanyT.FindAsync(c => c.UserId == int.Parse(userId));
        }

        // GET: api/Companies/5
        [HttpGet("{id}", Name = "Get")]
        public async Task<ActionResult<Company>> Get(int id)
        {
            var company = await _asyncRepositoryCompany.GetCompanyWithDepartamentsAsync(id);

            if (company == null)
            {
                return NotFound();
            }
            else
            {
                return company;
            }
        }

        // POST: api/Companies
        [HttpPost]
        public async Task<IActionResult> Post(CompanyDto companyForCreateDto)
        {
            Company companyForCreate = new Company();
            companyForCreate.Name = companyForCreateDto.Name;
            companyForCreate.UserId = companyForCreateDto.UserId;
            await _asyncRepositoryCompanyT.AddAsync(companyForCreate);
            return StatusCode((int)HttpStatusCode.Created);
        }

        // PUT: api/Company/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, CompanyDto companyDto)
        {
            if (companyDto.Id != id)
            {
                return BadRequest();
            }

            var companyFromDb = await _asyncRepositoryCompanyT.GetByIdAsync(id);

            if (companyFromDb == null)
            {
                return NotFound();
            }

            companyFromDb.Name = companyDto.Name;

            await _asyncRepositoryCompanyT.UpdateAsync(companyFromDb);

            return Ok("Company update");
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var companyFromDb = await _asyncRepositoryCompanyT.GetByIdAsync(id);

            if (companyFromDb == null)
            {
                return NotFound();
            }

            await _asyncRepositoryCompanyT.DeleteAsync(companyFromDb);
            return Ok("Company deleted");
        }
    }
}
