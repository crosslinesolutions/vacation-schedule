﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Threading.Tasks;
using VacationSchedule.Core.Entities;
using VacationSchedule.Core.Interfaces;
using VacationSchedule.Dto;

namespace VacationSchedule.WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class DepartmentsController : ControllerBase
    {
        private readonly IDepartamentRepository _departamentReoisitory;
        private readonly IAsyncRepository<Department>
            _asyncDepartamenrGenericRepository;
        public DepartmentsController(IAsyncRepository<Department> asyncDepartamenrGenericRepository, IDepartamentRepository departamentReoisitory)
        {
            _asyncDepartamenrGenericRepository = asyncDepartamenrGenericRepository;
            _departamentReoisitory = departamentReoisitory;
        }



        // GET: api/Departments/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Department>> Get(int id)
        {
            var department = await _departamentReoisitory.GetDepartamentWhithEmployeesById(id);

            if (department == null)
            {
                return NotFound();
            }

            return department;
        }

        // PUT: api/Departments/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, DepartamentForUpdate departmentDto)
        {
            if (id != departmentDto.Id)
            {
                return BadRequest();
            }

            var departmentFromDb = await _asyncDepartamenrGenericRepository.GetByIdAsync(id);

            if (departmentFromDb == null)
            {
                return NotFound();
            }

            await _asyncDepartamenrGenericRepository.UpdateAsync(departmentFromDb);

            return Ok("Departament updated");

        }

        // POST: api/Departments
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Department>> Post(DepartamentDto departmentDtp)
        {

            Department departamentToCreate = new Department()
            {
                Name = departmentDtp.Name,
                Description = departmentDtp.Description,
                CompanyId = departmentDtp.CompanyId
            };

            await _asyncDepartamenrGenericRepository.AddAsync(departamentToCreate);

            return StatusCode((int)HttpStatusCode.Created);
        }

        // DELETE: api/Departments/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Department>> DeleteDepartment(int id)
        {
            var department = await _asyncDepartamenrGenericRepository.GetByIdAsync(id);
            if (department == null)
            {
                return NotFound();
            }

            await _asyncDepartamenrGenericRepository.DeleteAsync(department);


            return Ok("Departament deleted");
        }

    }
}
