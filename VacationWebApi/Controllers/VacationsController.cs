﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Net;
using System.Threading.Tasks;
using VacationSchedule.Core.Entities;
using VacationSchedule.Core.Interfaces;
using VacationSchedule.WebApi.CustomValidation;
using VacationSchedule.WebApi.Interfaces;

namespace VacationSchedule.WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class VacationsController : ControllerBase
    {
        private readonly IAsyncRepository<Vacation> _asyncGenericRepository;
        private readonly IVacationService _vacationService;
        private readonly IVacationRepository _vacationRepository;

        public VacationsController(IAsyncRepository<Vacation> asyncGenericRepository, IVacationService vacationService, IVacationRepository vacationRepository)
        {
            _asyncGenericRepository = asyncGenericRepository;
            _vacationService = vacationService;
            _vacationRepository = vacationRepository;
        }


        // GET: api/Vacations/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Vacation>> Get(int id)
        {
            var vacation = await _vacationRepository.GetVacationById(id);

            return vacation;
        }

        [HttpPost]
        public async Task<IActionResult> Post(VacationDto vacationDto)
        {
            var errors = await _vacationService.VaidateCrossDays(vacationDto);

            if (errors.Count > 0)
            {
                return BadRequest(new
                {
                    Errors = errors
                });
            }

            await _vacationService.CreateVacation(vacationDto);

            return StatusCode((int)HttpStatusCode.Created);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(VacationDto vacationDto, int vacationId)
        {
            await _vacationService.UpdateVacation(vacationDto, vacationId);

            return Ok("Vacation upadted");
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var vacation = await _asyncGenericRepository.GetByIdAsync(id);
            if (vacation == null)
            {
                return NotFound("Vacation not found");
            }

            try
            {
                await _asyncGenericRepository.DeleteAsync(vacation);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

            return Ok("Vacation deleted");
        }
    }
}