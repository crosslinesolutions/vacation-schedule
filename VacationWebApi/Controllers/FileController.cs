﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Entities;
using Core.Interfaces;
using Dto;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using VacationSchedule.Core.Entities;
using VacationSchedule.Core.Interfaces;
using VacationSchedule.Dto;
using VacationSchedule.WebApi.Interfaces;
using VacationSchedule.WebApi.ReadFile;

namespace VacationSchedule.WebApi.Controllers
{

    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class FileController : ControllerBase
    {
        private IFileService _fileService;
        private IEmployeesService _employeesService;

        public FileController(IFileService uploadService, IEmployeesService employeesService)
        {
            _fileService = uploadService;
            _employeesService = employeesService;
        }

        [HttpPost, DisableRequestSizeLimit]
        [Route("upload")]
        public async Task<IActionResult> UploadFile([FromForm] UploadFileDto dto)
        {

            var resErrors = await _fileService.ValidateFile(Request.Form.Files[0]);

            if (resErrors.Count > 0)
            {
                return BadRequest(resErrors);
            }
            else
            {
                try
                {
                    string path = await _fileService.SaveFile(Request.Form.Files[0]);

                    var context = new ReadFileContext(new ReadEcxelFile());
                    context.Path = path;
                    context.DepartamentId = dto.DepartamentId;


                    List<Employee> employeesFomFile = context.Execute();

                    if (employeesFomFile.Count > 0)
                    {
                        await _employeesService.LoadData(employeesFomFile);
                    }

                    //удаление файла после загрузки данных в базу
                    _fileService.DeleteFile(path);
                    
                    return Ok("File uploaded");
                }
                catch (Exception e)
                {
                    return BadRequest("file write error");

                }
            }


        }
    }
}