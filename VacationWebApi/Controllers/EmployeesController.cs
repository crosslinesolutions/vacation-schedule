﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Threading.Tasks;
using VacationSchedule.Core.Entities;
using VacationSchedule.Core.Interfaces;
using VacationSchedule.Dto;

namespace VacationSchedule.WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeesController : ControllerBase
    {

        private readonly IAsyncRepository<Employee> _asyncEmployeesGeneticRepository;
        private readonly IEmployeesRepository _employeesRepository;
        public EmployeesController(IAsyncRepository<Employee> asyncEmployeesGenericRepository, IEmployeesRepository employeesRepository)
        {
            _asyncEmployeesGeneticRepository = asyncEmployeesGenericRepository;
            _employeesRepository = employeesRepository;
        }



        // GET: api/Employees/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Employee>> Get(int id)
        {
            var employee = await _employeesRepository.GetEmployeeById(id);

            if (employee == null)
            {
                return NotFound();
            }

            return employee;
        }

        // PUT: api/Employees/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, EmployeeDto employeeDto)
        {
            if (id != employeeDto.Id)
            {
                return BadRequest();
            }

            Employee fromDb = await _asyncEmployeesGeneticRepository.GetByIdAsync(id);

            fromDb.DepartamentId = employeeDto.DepartamentId;
            fromDb.FirstName = employeeDto.FirstName;
            fromDb.LastName = employeeDto.LastName;
            fromDb.Position = employeeDto.Position;

            await _asyncEmployeesGeneticRepository.UpdateAsync(fromDb);

            return NoContent();
        }

        // POST: api/Employees
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Employee>> Post(EmployeeToCreateDto employeeDto)
        {
            Employee employeeToCreate = new Employee()
            {
                FirstName = employeeDto.FirstName,
                LastName = employeeDto.LastName,
                Position = employeeDto.Position,
                DepartamentId = employeeDto.DepartamentId
            };

            await _asyncEmployeesGeneticRepository.AddAsync(employeeToCreate);
            return StatusCode((int)HttpStatusCode.Created);
        }

        // DELETE: api/Employees/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Employee>> Delete(int id)
        {
            var employee = await _asyncEmployeesGeneticRepository.GetByIdAsync(id);
            if (employee == null)
            {
                return NotFound();
            }

            await _asyncEmployeesGeneticRepository.DeleteAsync(employee);

            return Ok("Employee deleted");
        }

    }
}
