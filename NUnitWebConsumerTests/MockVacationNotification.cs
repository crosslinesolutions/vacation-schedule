﻿using System;
using System.Collections.Generic;
using System.Text;
using VacationSchedule.Core.Notifications;

namespace NUnitWebConsumerTests
{
    internal class MockVacationNotification : IVacationNotification
    {
        public DateTime CreateDate { get; set; }
        public IEnumerable<VacationInfo> Vacations { get; set; }
    }
}
