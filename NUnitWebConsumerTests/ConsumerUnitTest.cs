using Microsoft.Extensions.Configuration;
using Moq;
using NUnit.Framework;
using NUnitWebConsumerTests;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using VacationSchedule.Core.Interfaces;
using VacationSchedule.Core.Notifications;
using VacationSchedule.Core.Services;

namespace WebConsumerComponentsNUnitTests
{
    public class Tests
    {
        private IFileWriter _fileWriter;

        [OneTimeSetUp]
        public void Setup()
        {
            var configuration = new Mock<IConfiguration>();
            var fileExtantionConfigValue = new Mock<IConfigurationSection>();
            var filePathConfigValue = new Mock<IConfigurationSection>();

            fileExtantionConfigValue.Setup(x => x.Value).Returns(".txt");
            filePathConfigValue.Setup(x => x.Value).Returns("C:\\WorkTemp\\");

            configuration.Setup(x => x.GetSection(It.Is<string>(s=> s == "FileExtantion"))).Returns(fileExtantionConfigValue.Object);
            configuration.Setup(x => x.GetSection(It.Is<string>(s => s == "FilePath"))).Returns(filePathConfigValue.Object);

            var notificationParser = new NotificationParser();
            _fileWriter = new FileWriter(configuration.Object, notificationParser);
        }

        [Test]
        public async Task FileWriterTest()
        {
            var message = new MockVacationNotification()
            {
                CreateDate = DateTime.Now,
                Vacations = new List<VacationInfo>()
            };
            var path = await _fileWriter.WriteNotificationToFile(message);

            Assert.IsNotNull(path);

            if (File.Exists(path))
            {
                File.Delete(path);
            }
        }
    }
}