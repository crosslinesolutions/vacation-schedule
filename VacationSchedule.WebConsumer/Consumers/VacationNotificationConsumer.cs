﻿using MassTransit;
using System.Threading.Tasks;
using VacationSchedule.Core.Notifications;
using VacationSchedule.Core.Interfaces;

namespace VacationSchedule.WebConsumer.Consumers
{
    public class VacationNotificationConsumer: IConsumer<IVacationNotification>
    {
        private readonly IFileWriter _fileWriter;

        public VacationNotificationConsumer(IFileWriter fileWriter)
        {
            _fileWriter = fileWriter;
        }

        public async Task Consume(ConsumeContext<IVacationNotification> context)
        {
            var message = context.Message;

            await _fileWriter.WriteNotificationToFile(message);
        }
    }
}
