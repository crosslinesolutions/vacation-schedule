using System;
using MassTransit;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using VacationSchedule.Core.MassTransit;
using VacationSchedule.Core.Services;
using VacationSchedule.Core.Interfaces;
using VacationSchedule.WebConsumer.Consumers;

namespace VacationSchedule.WebConsumer
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            //MassTransit
            services.Configure<MassTransitConfiguration>(Configuration.GetSection("MassTransit"));
            services.AddMassTransit(x =>
            {
                x.AddConsumer<VacationNotificationConsumer>();

                x.AddBus(isp => Bus.Factory.CreateUsingRabbitMq(cfg =>
                {
                    var hostConfig = isp.GetRequiredService<IOptions<MassTransitConfiguration>>().Value;
                    cfg.Host(new Uri(hostConfig.RabbitMqAddress + "/"), h =>
                    {
                        h.Username(hostConfig.UserName);
                        h.Password(hostConfig.Password);
                    });

                    cfg.ReceiveEndpoint(Configuration.GetValue<string>("RabbitQueueName"), ep =>
                    {
                        ep.ConfigureConsumer<VacationNotificationConsumer>(isp);
                    });
                }));
            });
            services.AddMassTransitHostedService();

            services.AddSingleton<IFileWriter, FileWriter>();
            services.AddSingleton<INotificationParser, NotificationParser>();

        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGet("/", async context =>
                {
                    await context.Response.WriteAsync("Hello World!");
                });
            });
        }
    }
}
