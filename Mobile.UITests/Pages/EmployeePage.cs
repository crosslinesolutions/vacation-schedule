﻿using Query = System.Func<Xamarin.UITest.Queries.AppQuery, Xamarin.UITest.Queries.AppQuery>;

namespace Mobile.UITests.Pages
{
    public class EmployeePage : BasePage
    {
        protected override PlatformQuery Trait => new PlatformQuery
        {
            Android = x => x.Id("PageLogin_loginField"),
        };
    }
}
