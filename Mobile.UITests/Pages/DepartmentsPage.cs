﻿using Query = System.Func<Xamarin.UITest.Queries.AppQuery, Xamarin.UITest.Queries.AppQuery>;

namespace Mobile.UITests.Pages
{
    public class DepartmentsPage : BasePage
    {
        readonly Query Department;
        readonly Query CreateDepartmentButton;
        readonly Query NewDepartmentNameField;

        protected override PlatformQuery Trait => new PlatformQuery
        {
            Android = x => x.Id("PageDepartments_pageHeader"),
        };

        public DepartmentsPage()
        {
            if (OnAndroid)
            {
                Department = x => x.Id("PageDepartmentInList_Layout");
                CreateDepartmentButton = x => x.Id("PageDepartments_CreateDepartment");
                NewDepartmentNameField = x => x.Id("PageDepartments_NewDepartmentName");
            }
        }

        public DepartmentsPage CreateDepartment(string depName)
        {
            app.Tap(CreateDepartmentButton);
            app.Tap(NewDepartmentNameField);
            app.EnterText(depName);
            app.Tap(CreateDepartmentButton);
            return this;
        }

        public void OpenDepartment()
        {
            app.Tap(Department);
        }
    }
}
