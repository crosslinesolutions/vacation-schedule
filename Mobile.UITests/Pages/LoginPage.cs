﻿using Query = System.Func<Xamarin.UITest.Queries.AppQuery, Xamarin.UITest.Queries.AppQuery>;

namespace Mobile.UITests.Pages
{
    public class LoginPage : BasePage
    {
        readonly Query LogInButton;
        readonly Query LoginField;
        readonly Query PasswordField;

        protected override PlatformQuery Trait => new PlatformQuery
        {
            Android = x => x.Id("PageLogin_loginField"),
            IOS = x => x.Id("notImplemented.png")
        };

        public LoginPage()
        {
            if (OnAndroid)
            {
                LogInButton = x => x.Id("PageLogin_loginButton");
                LoginField = x => x.Id("PageLogin_loginField");
                PasswordField = x => x.Id("PageLogin_passwordField");
            }
        }

        public void SignIn_SignUp(string login, string password)
        {
            app.Tap(LoginField);
            app.EnterText(login);
            app.Tap(PasswordField);
            app.EnterText(password);
            app.Tap(LogInButton);
        }
    }
}
