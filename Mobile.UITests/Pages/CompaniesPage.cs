﻿using Query = System.Func<Xamarin.UITest.Queries.AppQuery, Xamarin.UITest.Queries.AppQuery>;

namespace Mobile.UITests.Pages
{
    public class CompaniesPage : BasePage
    {
        readonly Query Company;

        protected override PlatformQuery Trait => new PlatformQuery
        {
            Android = x => x.Id("PageAccount_pageTitle"),
        };

        public CompaniesPage()
        {
            Company = x => x.Id("PageOrganicationInList_Layout");
        }

        public void SelectCompany()
        {
            app.Tap(Company);
        }
    }
}
