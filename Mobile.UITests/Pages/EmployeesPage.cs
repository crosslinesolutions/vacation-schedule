﻿using Query = System.Func<Xamarin.UITest.Queries.AppQuery, Xamarin.UITest.Queries.AppQuery>;


namespace Mobile.UITests.Pages
{
    public class EmployeesPage : BasePage
    {
        readonly Query Employee;
        readonly Query CreateEmployeeButton;
        readonly Query NewEmployeeFirstName;
        readonly Query NewEmployeeSecondName;

        protected override PlatformQuery Trait => new PlatformQuery
        {
            Android = x => x.Id("PageEmployees_pageTitle"),
        };


        public EmployeesPage()
        {
            if (OnAndroid)
            {
                Employee = x => x.Id("PageEmployeeInOrganization_layout");
                CreateEmployeeButton = x => x.Id("PageEmployees_createNewButton");
                NewEmployeeFirstName = x => x.Id("PageEmployees_newFirstName");
                NewEmployeeSecondName = x => x.Id("PageEmployees_newSecondName");
            }
        }

        public EmployeesPage CreateEmployee(string fisrtName, string secondName)
        {
            app.Tap(CreateEmployeeButton);
            app.Tap(NewEmployeeFirstName);
            app.EnterText(fisrtName);
            app.Tap(NewEmployeeSecondName);
            app.EnterText(secondName);
            app.Tap(CreateEmployeeButton);
            return this;
        }

        public void OpenEmployee()
        {
            app.Tap(Employee);
        }
    }
}
