﻿using Query = System.Func<Xamarin.UITest.Queries.AppQuery, Xamarin.UITest.Queries.AppQuery>;

namespace Mobile.UITests.Pages
{
    public class MainPage : BasePage
    {
        readonly Query LogInButton;
        readonly Query SignUpButton;

        protected override PlatformQuery Trait => new PlatformQuery
        {
            Android = x => x.Id("loginButton"),
            IOS = x => x.Id("notImplemented.png")
        };

        public MainPage()
        {
            if (OnAndroid)
            {
                LogInButton = x => x.Marked("Login");
                SignUpButton = x => x.Marked("Регистрация");
            }
        }

        public void SignIn()
        {
            app.Tap(LogInButton);
        }

        public void SignUp()
        {
            app.Tap(SignUpButton);
        }

    }
}
