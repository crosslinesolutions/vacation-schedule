﻿using System;
using Xamarin.UITest;

namespace Mobile.UITests
{
    static class AppManager
    {
        static IApp app;
        public static IApp App
        {
            get
            {
                if (app == null)
                    throw new
                        NullReferenceException();
                return app;
            }
        }
        static Platform? platform;
        public static Platform Platform
        {
            get
            {
                if (platform == null)
                    throw new
                        NullReferenceException();
                return platform.Value;
            }
            set
            {
                platform = value;
            }
        }

        public static void StartApp()
        {
            if (Platform == Platform.Android)
                app = ConfigureApp
                    .Android
                    .EnableLocalScreenshots()
                    .ApkFile(@"C:\Users\evgen\Source\Repos\vacation-schedule\Mobile.Android\bin\Debug\com.companyname.mobile.android.apk")
                    .StartApp(Xamarin.UITest.Configuration.AppDataMode.DoNotClear);
            if (Platform == Platform.iOS)
                app = ConfigureApp
                    .iOS
                    .EnableLocalScreenshots()
                    .InstalledApp("ru.ggggggg")
                    .StartApp();
        }
    }
}