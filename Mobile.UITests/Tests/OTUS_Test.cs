﻿using Mobile.UITests.Pages;
using NUnit.Framework;
using System;
using Xamarin.UITest;

namespace Mobile.UITests.Tests
{
    public class OTUS_Test : BaseTestFixture
    {
        public OTUS_Test(Platform platform) : base(platform)
        {
        }

        [Test]
        public void JustOneExampleTest()
        {
            string login = "123";
            string password = "12345678";

            new MainPage()
                .SignIn();
            Console.WriteLine(app.Screenshot("OTUS_TEST_" + DateTime.Now));
            new LoginPage()
                .SignIn_SignUp(login, password);
            Console.WriteLine(app.Screenshot("OTUS_TEST_" + DateTime.Now));
            new CompaniesPage()
                .SelectCompany();
            Console.WriteLine(app.Screenshot("OTUS_TEST_" + DateTime.Now));
            new DepartmentsPage()
                .CreateDepartment("TEST_DEPArt" + DateTime.Now.ToShortTimeString())
                .OpenDepartment();
            new EmployeesPage()
                .CreateEmployee("Liam", "Payne")
                .OpenEmployee();
        }
    }
}