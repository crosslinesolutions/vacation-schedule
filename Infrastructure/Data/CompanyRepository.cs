﻿using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using VacationSchedule.Core.Entities;
using VacationSchedule.Core.Interfaces;

namespace VacationSchedule.Infrastructure.Data
{
    public class CompanyRepository : ICompanyRepository
    {
        private VacationContext _context;

        public CompanyRepository(VacationContext context)
        {
            _context = context;
        }

        public async Task<Company> GetCompanyWithDepartamentsAsync(int departamentId)
        {
            return await _context.Companies.Include(d => d.Departments)
                .FirstOrDefaultAsync(d => d.Id == departamentId);
        }
    }
}
