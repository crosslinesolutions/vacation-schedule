﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VacationSchedule.Core.Entities;
using VacationSchedule.Core.Interfaces;

namespace VacationSchedule.Infrastructure.Data
{
    public class VacationRepository : IVacationRepository
    {
        private readonly VacationContext _context;

        public VacationRepository(VacationContext context)
        {
            _context = context;
        }

        public VacationRepository()
        {
        }

        public async Task<Vacation> GetVacationById(int id)
        {
            return await _context.VacationStatments.Include(e => e.Employee).Include(v => v.VacationDays).FirstOrDefaultAsync(x => x.Id == id);
        }

        public List<Vacation> GetAllVacationStartNDaysBefore(int daysBefore)
        {
            return _context.VacationStatments.Include(e => e.Employee).Where(x => x.VacationStart.Subtract(DateTime.Now).Days >= daysBefore).ToList();
        }

        public async Task<List<Vacation>> GetAllVacationInDepartament(int departamentId)
        {

            return await _context.VacationStatments.Include(vd => vd.VacationDays).ThenInclude(v => v.Vacation)
                .Where(d => d.Employee.DepartamentId == departamentId).ToListAsync();
        }
    }
}
