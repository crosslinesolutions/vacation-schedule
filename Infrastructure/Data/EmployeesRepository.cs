﻿using System.Threading.Tasks;
using Core.Entities;
using Core.Interfaces;
using Microsoft.EntityFrameworkCore;
using VacationSchedule.Core.Entities;
using VacationSchedule.Core.Interfaces;

namespace VacationSchedule.Infrastructure.Data
{
   public class EmployeesRepository : IEmployeesRepository
    {
        private VacationContext _context;

        public EmployeesRepository(VacationContext context)
        {
            _context = context;
        }


        public async Task<Employee> GetEmployeeById(int id)
        {
            return await _context.Employees.Include(d => d.Departament).ThenInclude(c=>c.Company)
                .Include(v => v.Vacations).ThenInclude(d => d.VacationDays).FirstOrDefaultAsync(i=>i.Id == id);
        }
    }
}
