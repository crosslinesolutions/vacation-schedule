﻿using Core.Entities;
using Microsoft.EntityFrameworkCore;
using Serilog;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Threading.Tasks;
using VacationSchedule.Core.Entities;

namespace VacationSchedule.Infrastructure.Data
{
    public class VacationContextSeed
    {
        public static async Task SeedAsync(VacationContext dbContext, int? retry = 0)
        {
            int retryForAvailability = retry.Value;

            try
            {
                // Only run this if using a real database
                //await dbContext.Database.MigrateAsync();

                if (!await dbContext.VacationType.AnyAsync())
                {
                    await dbContext.VacationType.AddRangeAsync(
                        GetPreconfigureVacationTypes());
                    await dbContext.SaveChangesAsync();
                }

                if (!await dbContext.Users.AnyAsync())
                {
                    await dbContext.Users.AddAsync(GetPreconfigureUsers());
                    await dbContext.SaveChangesAsync();
                }

                if (!await dbContext.VacationStatments.AnyAsync())
                {
                    var res = GetPreconfigureVacations();
                    await dbContext.VacationStatments.AddRangeAsync(res);
                    await dbContext.SaveChangesAsync();
                }

            }
            catch (Exception ex)
            {
                if (retryForAvailability < 10)
                {
                    retryForAvailability++;
                    Log.Error(ex, ex.Message);
                    await SeedAsync(dbContext, retryForAvailability);
                }
            }
        }

        private static List<Vacation> GetPreconfigureVacations()
        {
            return new List<Vacation>()
            {
                new Vacation()
                {
                    EmployeeId = 1,
                    VacatyionTypeId = 1,
                    VacationStart = new DateTime(2020, 5, 18),
                    VacationEnd = new DateTime(2020, 5, 21),
                    Status = Vacation.VacationStatus.OnMatching,
                    TotalVacationDays = 4,
                    VacationDays = new List<VacationDay>()
                    {
                        new VacationDay()
                        {
                            IsWorkDay = false, Dt = new DateTime(2020, 5, 18)
                        },
                        new VacationDay()
                        {

                            IsWorkDay = false, Dt = new DateTime(2020, 5, 19)
                        },
                        new VacationDay()
                        {

                            IsWorkDay = false, Dt = new DateTime(2020, 5, 20)
                        },
                        new VacationDay()
                        {
                            IsWorkDay = true, Dt = new DateTime(2020, 5, 21)
                        }
                    }
                },
                new Vacation()
                {
                    EmployeeId = 4,
                    VacatyionTypeId = 1,
                    VacationStart = new DateTime(2020, 5, 18),
                    VacationEnd = new DateTime(2020, 5, 21),
                    Status = Vacation.VacationStatus.OnMatching,
                    TotalVacationDays = 4,
                    VacationDays = new List<VacationDay>()
                    {
                        new VacationDay()
                        {
                            IsWorkDay = true, Dt = new DateTime(2020, 5, 18)
                        },
                        new VacationDay()
                        {
                            IsWorkDay = true, Dt = new DateTime(2020, 5, 19)
                        },
                        new VacationDay()
                        {

                            IsWorkDay = true, Dt = new DateTime(2020, 5, 20)
                        },
                        new VacationDay()
                        {
                            IsWorkDay = true, Dt = new DateTime(2020, 5, 21)
                        }
                    }
                },
                new Vacation()
                {
                    EmployeeId = 2,
                    VacatyionTypeId = 1,
                    VacationStart = new DateTime(2020, 8, 1),
                    VacationEnd = new DateTime(2020, 8, 5),
                    Status = Vacation.VacationStatus.Creation,
                    TotalVacationDays = 4,
                    VacationDays = new List<VacationDay>()
                    {
                        new VacationDay()
                        {
                            IsWorkDay = true, Dt = new DateTime(2020, 8, 1)
                        },
                        new VacationDay()
                        {
                            IsWorkDay = true, Dt = new DateTime(2020, 8, 2)
                        },
                        new VacationDay()
                        {
                            IsWorkDay = true, Dt = new DateTime(2020, 8, 3)
                        },
                        new VacationDay()
                        {
                            IsWorkDay = true, Dt = new DateTime(2020, 8, 4)
                        }
                    }
                },
                new Vacation()
                {
                    EmployeeId = 3,
                    VacatyionTypeId = 1,
                    VacationStart = new DateTime(2020, 5, 17),
                    VacationEnd = new DateTime(2020, 5, 20),
                    Status = Vacation.VacationStatus.Creation,
                    TotalVacationDays = 4,
                    VacationDays = new List<VacationDay>()
                    {
                        new VacationDay()
                        {
                            IsWorkDay = true, Dt = new DateTime(2020, 5, 17)
                        },
                        new VacationDay()
                        {
                            IsWorkDay = true, Dt = new DateTime(2020, 5, 18)
                        },
                        new VacationDay()
                        {
                            IsWorkDay = true, Dt = new DateTime(2020, 5, 19)
                        },
                        new VacationDay()
                        {
                            IsWorkDay = true, Dt = new DateTime(2020, 5, 20)
                        }
                    }
            }
            };
        }

        private static List<VacationType> GetPreconfigureVacationTypes()
        {
            return new List<VacationType>()
            {
                new VacationType()
                {
                    Name = "Обязательный",
                    Description = "Очередной оплачиваемый отпуск"

                },
                new VacationType()
                {
                    Name = "За свой счёт",
                    Description = "отпуск без сохранения заработной платы"
                }
            };
        }
        private static List<Department> GetPreconfigureDepartaments()
        {
            return new List<Department>()
            {
                new Department()
                {
                    Name = "Sales",
                    Description = "Продают товары и услуги",
                    CompanyId = 1,
                    SameTimePeopleOnVacation = 2,
                    VacationDaysRule = 20,
                    Employees = new List<Employee>()
                    {
                        new Employee()
                        {
                            FirstName = "TestName",
                            LastName = "TestLastName",
                            Position = "Sale manager"

                        },
                        new Employee()
                        {
                            DepartamentId = 1,
                            FirstName = "TestName",
                            LastName = "TestLastName",
                            Position = "Sale manager",

                        },
                        new Employee()
                        {
                            DepartamentId = 1,
                            FirstName = "TestName",
                            LastName = "TestLastName",
                            Position = "Sale manager",
                        }
                    }

                },
                new Department()
                {
                    Name = "Teahers",
                    Description = "Преподавательский состав",
                    CompanyId = 1,
                    SameTimePeopleOnVacation = 2,
                    VacationDaysRule = 20,
                    Employees = new List<Employee>()
                    {
                        new Employee()
                        {
                            DepartamentId = 2,
                            FirstName = "TestName",
                            LastName = "TestLastName",
                            Position = "Teacher 4"

                        },
                        new Employee()
                        {
                            DepartamentId = 2,
                            FirstName = "TestName",
                            LastName = "TestLastName",
                            Position = "Teacher 3"

                        },new Employee()
                        {
                            DepartamentId = 2,
                            FirstName = "TestName",
                            LastName = "TestLastName",
                            Position = "Teacher 2"

                        },
                        new Employee()
                        {
                            DepartamentId = 2,
                            FirstName = "TestName",
                            LastName = "TestLastName",
                            Position = "Teacher 1"

                        }
                    }
                },
                new Department()
                {

                    Name = "Finance",
                    Description = "Управление финансами",
                    CompanyId = 1,
                    SameTimePeopleOnVacation = 1,
                    VacationDaysRule = 28,
                    Employees = new List<Employee>()
                    {
                        new Employee()
                        {
                            DepartamentId = 3,
                            FirstName = "TestName",
                            LastName = "TestLastName",
                            Position = "Chief Accountant"

                        },
                        new Employee()
                        {
                            DepartamentId = 3,
                            FirstName = "TestName",
                            LastName = "TestLastName",
                            Position = "Accountant"

                        },
                        new Employee()
                        {
                            DepartamentId = 3,
                            FirstName = "TestName",
                            LastName = "TestLastName",
                            Position = "Boss"

                        }
                    }
                }
            };
        }
        private static List<Company> GetPreconfigureCompanies()
        {
            return new List<Company>()
            {
                new Company()
                {
                    Name = "Otus",
                    UserId = 1,
                    Departments = GetPreconfigureDepartaments()
                }
            };
        }

        private static User GetPreconfigureUsers()
        {
            byte[] passwordHash;
            byte[] passwordSalt;

            using (var hmac = new HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes("12345678"));
            }

            return new User()
            {
                PasswordHash = passwordHash,
                PasswordSalt = passwordSalt,
                Username = "123",
                Companies = GetPreconfigureCompanies()
            };
        }

    }
}
