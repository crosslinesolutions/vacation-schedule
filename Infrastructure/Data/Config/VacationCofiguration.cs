﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using VacationSchedule.Core.Entities;

namespace VacationSchedule.Infrastructure.Data.Config
{
    class VacationCofiguration : IEntityTypeConfiguration<Vacation>
    {
        public void Configure(EntityTypeBuilder<Vacation> builder)
        {
            builder.ToTable("Vacations");
            builder.HasKey(v => v.Id);
            builder.HasOne(e => e.Employee).WithMany(v => v.Vacations)
                .HasForeignKey(e => e.EmployeeId)
                .OnDelete(DeleteBehavior.Cascade);

        }
    }
}
