﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using VacationSchedule.Core.Entities;

namespace VacationSchedule.Infrastructure.Data.Config
{
    class VacationDayConfiguration : IEntityTypeConfiguration<VacationDay>
    {
        public void Configure(EntityTypeBuilder<VacationDay> builder)
        {
            builder.ToTable("VacationDays");
            builder.HasKey(d => d.Id);
            builder.HasOne(v => v.Vacation).WithMany(vd => vd.VacationDays)
                .HasForeignKey(vd => vd.VacationId)
                .OnDelete(DeleteBehavior.Cascade);
          
        }
    }
}
