﻿using Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using VacationSchedule.Core.Entities;

namespace VacationSchedule.Infrastructure.Data.Config
{
    class EmployeeConfiguration : IEntityTypeConfiguration<Employee>
    {
        public void Configure(EntityTypeBuilder<Employee> builder)
        {
            builder.ToTable("Employees");
            builder.HasKey(c => c.Id);
            builder.HasOne(d => d.Departament)
                .WithMany(e => e.Employees)
                .HasForeignKey(d => d.DepartamentId)
                .OnDelete(DeleteBehavior.Cascade);
            
        }
    }
}
