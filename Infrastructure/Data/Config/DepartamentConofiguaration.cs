﻿using Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using VacationSchedule.Core.Entities;

namespace VacationSchedule.Infrastructure.Data.Config
{
    class DepartamentConofiguaration : IEntityTypeConfiguration<Department>
    {
        public void Configure(EntityTypeBuilder<Department> builder)
        {
            builder.ToTable("Departaments");
            builder.HasKey(d => d.Id);
            builder.HasOne(c => c.Company)
                .WithMany(d => d.Departments)
                .HasForeignKey(d => d.CompanyId);

        }
    }
}
