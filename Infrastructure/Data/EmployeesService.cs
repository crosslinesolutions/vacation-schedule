﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Entities;
using Core.Interfaces;
using VacationSchedule.Core.Entities;
using VacationSchedule.Core.Extensions;
using VacationSchedule.Core.Interfaces;

namespace VacationSchedule.Infrastructure.Data
{
    public class EmployeesService: IEmployeesService
    {
       
        private IAsyncRepository<Employee> _asyncRepository;
        public EmployeesService(IAsyncRepository<Employee> asyncRepository)
        {
            _asyncRepository = asyncRepository;
        }

        public async Task LoadData(List<Employee> data)
        {

            List<List<Employee>> Chunklists = data.ChunkBy(data.Count / 4);

            List<Task> tasks = new List<Task>();

            foreach (List<Employee> list in Chunklists)
            {
                tasks.Add(Task.Run((() => Load(list))));
            }
            await Task.WhenAll();

        }

        private void Load(List<Employee> data)
        {
             _asyncRepository.AddRangeDataAsync(data);
        }
    }
}
