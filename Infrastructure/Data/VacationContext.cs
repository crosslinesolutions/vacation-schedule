﻿using System.Reflection;
using Core.Entities;
using Microsoft.EntityFrameworkCore;
using VacationSchedule.Core.Entities;

namespace VacationSchedule.Infrastructure.Data
{
    public class VacationContext : DbContext
    {
        public VacationContext(DbContextOptions<VacationContext> options):base(options)
        {
            
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Company> Companies { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<VacationDay> VacationDays { get; set; }
        public DbSet<Vacation> VacationStatments { get; set; }
        public DbSet<VacationType> VacationType { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
        }

    }
}
