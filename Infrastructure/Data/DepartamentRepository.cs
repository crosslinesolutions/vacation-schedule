﻿using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using VacationSchedule.Core.Entities;
using VacationSchedule.Core.Interfaces;

namespace VacationSchedule.Infrastructure.Data
{
    public class DepartamentRepository : IDepartamentRepository
    {
        private VacationContext _context;

        public DepartamentRepository(VacationContext context)
        {
            _context = context;
        }

        public async Task<Department> GetDepartamentWhithEmployeesById(int departamentId)
        {
            return await _context.Departments.Include(e => e.Company)
                .Include(e => e.Employees)
                .ThenInclude(v=>v.Vacations).ThenInclude(vd=>vd.VacationDays)
                .FirstOrDefaultAsync(i => i.Id == departamentId);
        }
    }
}
