# VacationSchedule 
![N|Solid](https://res.cloudinary.com/dhzm89pfd/image/upload/v1589409232/logo2.png)
### Многопользовательская система учёта отпусков.
##### Основной функционал:
  - Регистрация и авторизация пользователей
  - Создание и наполнение компании отделами (департаментами) и сотрудниками
  - Загрузка сотрудников, как в ручном режиме, так и с помощью импорта данных из файла
  - Проверки на пересечение отпусков сотрудников внутри отдела и корректность выбора дат периода отпуска
  - Сервис проверки каждого дня отпуска на предмет - праздничный это день или нет, для учёта чистых дней отпуска
  - Предусмотрен выбор типа отпуска (оплачиваемый / за свой счёт)
  - Сервис напоминания о предстоящем отпуске сотрудника по средства отправки сообщения смс/email

  
##### Арихитектура
  - client–server
  - Srever:
  -- VacationSchedule.WebApi
  - Clients:
  -- Xamarin Android app
  -- Xamarin IOS app (условно)
  -- web client (не успели)

##### Технологии:
  - MsSql
  - EntityFrameworkCore
  - AspNetCore WebApi
  - MassTransit
  - RabbitMQ
  - Xamarin
  - Quartz
  - NUnit
  - XUnit


#####  Ссылка на коллекцию запросов в Postman для тестирования api:
https://www.getpostman.com/collections/ddaf3635c0731c876be6

### Installation

Для установки с использованием реальной базы данных изменить startap.cs проекта VacationSchedule.WebApi

```cs
  public void ConfigureDevelopmentServices(IServiceCollection services)
        {
            // use in-memory database
            ConfigureInMemoryDatabases(services);
            // use real database
            //ConfigureProductionServices(services);
        }
```
Далее раcкоментировать строку в файле Infrastructure.Data.VacationContextSeed.cs
```cs
 // Only run this if using a real database
 //await dbContext.Database.MigrateAsync();
```

Или применить миграцию в командной строке в папке проекта VacationSchedule.WebApi

```cs
dotnet-ef database update -c vacationcontext -p ../Infrastructure/VacationSchedule.Infrastructure.csproj -s VacationSchedule.WebApi.csproj 
```

####