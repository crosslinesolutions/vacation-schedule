﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using VacationSchedule.Infrastructure.Data;


namespace XUnitTestWebApi
{
    public class DbContextMocker
    {
        public static async Task<VacationContext> GetVacationContext()
        {
            var options = new DbContextOptionsBuilder<VacationContext>()
                .UseInMemoryDatabase(databaseName: "VacationDb").Options;
            // Create instance of DbContext
            var dbContext = new VacationContext(options);

            // Add entities in memory
            await VacationContextSeed.SeedAsync(dbContext);

            return dbContext;
        }
    }
}
