﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using VacationSchedule.Core.Entities;
using VacationSchedule.Core.Interfaces;
using VacationSchedule.WebApi.CustomValidation;
using VacationSchedule.Infrastructure.Data;
using VacationSchedule.WebApi.Interfaces;
using VacationSchedule.WebApi.Services;
using Xunit;

namespace XUnitTestWebApi
{
    public class ServicesUnitTest
    {
        private VacationContext _context;
        private IAsyncRepository<Vacation> _asyncGeneticRepositoryVacation;
        private IAsyncRepository<VacationDay> _asyncGenericRepositoryDays;
        private IVacationRepository _vacationRepository;
        private IEmployeesRepository _employeesRepository;


        [Fact]
        public async Task CreateVacation()
        {
            _context = DbContextMocker.GetVacationContext().Result;
            _asyncGeneticRepositoryVacation = new EfRepository<Vacation>(_context);
            _asyncGenericRepositoryDays = new EfRepository<VacationDay>(_context);
            _vacationRepository = new VacationRepository(_context);
            _employeesRepository = new EmployeesRepository(_context);
           
            IVacationService vacationService = new VacationService(_asyncGeneticRepositoryVacation, _asyncGenericRepositoryDays, _vacationRepository, _employeesRepository);


            VacationDto vacation = new VacationDto()
            {
                EmployeeId = 1,
                VacatyionTypeId = 1,
                VacationStart = new DateTime(2020, 9, 1),
                VacationEnd = new DateTime(2020, 9, 10),
            };
            bool result;
            try
            {
              await vacationService.CreateVacation(vacation);
              result = true;
            }
            catch (Exception e)
            {
                result = false;
            }
            Assert.True(result);

        }
    }
}
