using System;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using VacationSchedule.Core.Entities;
using VacationSchedule.Core.Interfaces;
using VacationSchedule.Dto;
using VacationSchedule.Infrastructure.Data;
using VacationSchedule.WebApi.Controllers;
using Xunit;

namespace XUnitTestWebApi
{
    public class VacationControllerUnitTest
    {
        private VacationContext _context;
        private ICompanyRepository _companyRepository;
        private IAsyncRepository<Company> _asyncGenericRepository;


        [Fact]
        public async Task GetCompanyByIdSuccess()
        {
            _context = DbContextMocker.GetVacationContext().Result;
            _companyRepository = new CompanyRepository(_context);
            _asyncGenericRepository = new EfRepository<Company>(_context);

            var controller = new CompaniesController(_asyncGenericRepository, _companyRepository);

            var response = await controller.Get(1);

            Assert.NotNull(response.Value);
        }

        [Fact]
        public async Task CreateCompanyReturn201StutusCode()
        {
            _context = DbContextMocker.GetVacationContext().Result;
            _companyRepository = new CompanyRepository(_context);
            _asyncGenericRepository = new EfRepository<Company>(_context);

            var controller = new CompaniesController(_asyncGenericRepository, _companyRepository);

            //act
            var response = await controller.Post(new CompanyDto()
            {
                Name = "Test Company Name",
                UserId = 1
            });
            var value = response as StatusCodeResult;
          
            Assert.Equal(201,value.StatusCode);
        }

    }
}
