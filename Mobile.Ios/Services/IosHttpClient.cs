﻿using Mobile.Core;
using Mobile.Core.Services;
using System;
using System.Threading.Tasks;

namespace Mobile.Ios.Services
{
    class IosHttpClient : IHttpClientService
    {
        public void ClearRequestHeaders()
        {
            throw new NotImplementedException();
        }

        public Task<(MessageCode, string)> GetToken(string url, string request)
        {
            throw new NotImplementedException();
        }

        public Task<(MessageCode Status, T Data)> HTTP<T>(HttpMethodType type, string url, string request = "")
        {
            throw new NotImplementedException();
        }

        public void RemoveHeader(string name)
        {
            throw new NotImplementedException();
        }

        public void SetBaseAddress(string url)
        {
            throw new NotImplementedException();
        }

        public void TryAddHeader(string name, string value)
        {
            throw new NotImplementedException();
        }
    }
}