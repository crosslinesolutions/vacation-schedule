﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using Mobile.Core.Services;
using Network;
using UIKit;

namespace Mobile.Ios.PlatformSpecific
{
    class NetworkMonitor
    {
        private readonly INetworkManager _networkManager;

        //public NetworkMonitor() => _networkManager = Mvx.IoCProvider.Resolve<INetworkManager>();

        public void Start()
        {
            if (UIDevice.CurrentDevice.CheckSystemVersion(12, 0))
            {
                NWPathMonitor monitor = new NWPathMonitor
                {
                    SnapshotHandler = (adapter) =>
                    {
                        if (adapter != null)
                        {
                            _networkManager.IsNetworkAvailable = adapter.Status == NWPathStatus.Satisfied || adapter.Status == NWPathStatus.Satisfiable;
                            Log.Event($"[NetworkMonitor] ConnectionStatus: {adapter.Status}. IPv4: {adapter.HasIPV4}. IPv6: {adapter.HasIPV6}. DNS: {adapter.HasDns}. {adapter.EffectiveLocalEndpoint}");
                        }
                    }
                };
                monitor.SetQueue(CoreFoundation.DispatchQueue.DefaultGlobalQueue);
                monitor.Start();
            }
            else
                _networkManager.IsNetworkAvailable = true;
        }
    }
}