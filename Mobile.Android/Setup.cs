﻿using Mobile.Android.PlatformSpecific;
using Mobile.Android.Services;
using Mobile.Android.Views.DepartmentPage;
using Mobile.Android.Views.EmployeesPage;
using Mobile.Android.Views.Fragments;
using Mobile.Android.Views.VacationPage;
using Mobile.Android.Views.VacationsPage;
using Mobile.Core;
using Mobile.Core.Services;
using Mobile.Core.Services.TimeService;
using Mobile.Core.ViewModels.AccountPage;
using Mobile.Core.ViewModels.CompanyPage;
using Mobile.Core.ViewModels.DepartmentPage;
using Mobile.Core.ViewModels.EmployeePage;
using Mobile.Core.ViewModels.LoginPage;
using Mobile.Core.ViewModels.VacationPage;
using MvvmCross;
using MvvmCross.Platforms.Android.Core;
using MvvmCross.ViewModels;
using System;
using System.Collections.Generic;

namespace Mobile.Android
{
    public class Setup : MvxAndroidSetup<App>
    {
        public Setup() : base()
        {
        }

        protected override IMvxApplication CreateApp()
        {
            return new App();
        }

        protected override void InitializeFirstChance()
        {
            base.InitializeFirstChance();
            Mvx.IoCProvider.RegisterSingleton<INetworkManager>(new NetworkManagerService());
            Mvx.IoCProvider.RegisterSingleton<IHttpClientService>(new AndroidHttpClient());
            Mvx.IoCProvider.RegisterSingleton<INtpTimeService>(new AndroidNtpTime());
            Mvx.IoCProvider.RegisterType<IDialogService, DialogService>();
        }

        public override void InitializeSecondary()
        {
            base.InitializeSecondary();
            NetworkSetup setup = new NetworkSetup();
            setup.RegisterNetworkCallBacks();
        }

        protected override IDictionary<Type, Type> InitializeLookupDictionary()
        {
            var viewModelViewLookup = new Dictionary<Type, Type>
            {
                {typeof(LoginPageVM), typeof(LoginActivity)},
                {typeof(AccountPageVM), typeof(AccountActivity) },
                {typeof(CompanyPageVM), typeof(DepartmentsActivity) },
                { typeof(DepartmentPageVM), typeof(EmplActivity)},
                {typeof(EmployeePageVM), typeof(VacationsActivity) },
                {typeof(VacationPageVM), typeof(VacationActivity) },
            };
            return viewModelViewLookup;
        }
    }
}