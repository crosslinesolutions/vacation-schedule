﻿using Android.App;
using Android.OS;
using Android.Widget;
using Mobile.Core.Services;

namespace Mobile.Android.Services
{
    public class DialogService : IDialogService
    {
        public void ShowMessage(string text)
        {
            using var mHandler = new Handler(Looper.MainLooper);
            mHandler.Post(() =>
            {
                Toast.MakeText(Application.Context, text, ToastLength.Long).Show();
            });
        }

    }
}