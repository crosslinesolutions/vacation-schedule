﻿using Mobile.Core.Services;
using System;

namespace Mobile.Android.Services
{
    public class NetworkManagerService : INetworkManager
    {
        public event EventHandler<bool> NetworkStatusChanged;
        private bool _isInternetAvailable;

        public bool IsNetworkAvailable
        {
            get => _isInternetAvailable;
            set
            {
                _isInternetAvailable = value;
                NetworkStatusChanged?.Invoke(this, _isInternetAvailable);
            }
        }
    }
}