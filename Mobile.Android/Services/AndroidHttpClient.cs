﻿using Java.Net;
using Mobile.Core;
using Mobile.Core.Services;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace Mobile.Android.Services
{
    public class AndroidHttpClient : IHttpClientService
    {
        private readonly string _defaultContentType = "application/json";
        private static readonly HttpClient HttpClient = new HttpClient();
        private readonly JsonSerializerOptions JsonOptions = new JsonSerializerOptions() { PropertyNameCaseInsensitive = true, IgnoreNullValues = true };

        public async Task<(MessageCode Status, T Data)> HTTP<T>(HttpMethodType type, string url, string serializedRequest = "")
        {
            using CancellationTokenSource requestTimedOutToken = new CancellationTokenSource(30_000);
            try
            {
                HttpResponseMessage response = null;
                switch (type)
                {
                    case HttpMethodType.Get:
                        response = await HttpClient.GetAsync(url, requestTimedOutToken.Token).ConfigureAwait(false);
                        break;
                    case HttpMethodType.Post:
                        StringContent content = new StringContent(serializedRequest, Encoding.UTF8, _defaultContentType);
                        response = await HttpClient.PostAsync(url, content, requestTimedOutToken.Token).ConfigureAwait(false);
                        break;
                }
                string responseText = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    Log.Warning($"[HttpClient] StatusCode: '{response.StatusCode}'. Content: '{responseText}'. URL: {HttpClient.BaseAddress + url}");
                    switch (response.StatusCode)
                    {
                        case HttpStatusCode.Unauthorized:
                            return (MessageCode.Unauthorized, default);
                        case HttpStatusCode.BadRequest:
                            return (MessageCode.BadRequest, default);
                        case HttpStatusCode.NotFound:
                            return (MessageCode.NotFound, default);
                        case HttpStatusCode.InternalServerError:
                            return (MessageCode.InternalServerError, default);
                        case HttpStatusCode.ServiceUnavailable:
                            return (MessageCode.ServiceUnavailable, default);
                        case HttpStatusCode.GatewayTimeout:
                            return (MessageCode.GatewayTimeout, default);
                        case HttpStatusCode.BadGateway:
                            return (MessageCode.BadGateway, default);
                        case HttpStatusCode.Created:
                            return (MessageCode.Ok, default);
                        default:
                            return (MessageCode.UndefinedHttpError, default(T));
                    }
                }
                return string.IsNullOrWhiteSpace(responseText) ? (MessageCode.Ok, default) : (MessageCode.Ok, JsonSerializer.Deserialize<T>(responseText, JsonOptions));
            }
            catch (UnknownHostException e)
            {
                Log.Warning($"[HttpClient] {e.Message}" + e);
                return (MessageCode.DomainNameError, default(T));
            }
            catch (HttpRequestException e)
            {
                Log.Warning($"[HttpClient] {e.Message}" + e);
                return (MessageCode.UndefinedNetworkError, default(T));
            }
            catch (WebException e)
            {
                Log.Warning($"[HttpClient] {e.Message}" + e);
                return (MessageCode.NetworkUnreachable, default(T));
            }
            catch (ConnectException e)
            {
                Log.Warning($"[HttpClient] {e.Message}" + e);
                return (MessageCode.ConnectionWasNotEstablished, default(T));
            }
            catch (NoRouteToHostException e)
            {
                Log.Warning($"[HttpClient] {e.Message}" + e);
                return (MessageCode.NoRouteToHost, default(T));
            }
            catch (Exception e)
            {
                if (requestTimedOutToken.IsCancellationRequested)
                    return (MessageCode.HttpTimeout, default(T));
                Log.Warning($"[HttpClient] {e.Message}" + e);
                return (MessageCode.UndefinedNetworkError, default(T));
            }
        }

        public async Task<(MessageCode, string)> GetToken(string url, string request)
        {
            using CancellationTokenSource requestTimedOutToken = new CancellationTokenSource(15_000);
            HttpResponseMessage httpResponse;
            try
            {
                httpResponse = await HttpClient.PostAsync(url, new StringContent(request, Encoding.UTF8, _defaultContentType)).ConfigureAwait(false);
                string content = await httpResponse.Content.ReadAsStringAsync().ConfigureAwait(false);
                if (httpResponse.StatusCode == HttpStatusCode.BadRequest)
                    return (MessageCode.BadRequest, content);
                if (httpResponse.StatusCode == HttpStatusCode.OK)
                    return (MessageCode.Ok, content);
                return (MessageCode.InternalServerError, string.Empty);
            }
            catch (UnknownHostException e)
            {
                Log.Warning($"[HttpClient] {e.Message}" + e);
                return (MessageCode.DomainNameError, string.Empty);
            }
            catch (Exception e)
            {
                if (requestTimedOutToken.IsCancellationRequested)
                    return (MessageCode.HttpTimeout, string.Empty);
                Log.Warning($"[HttpClient] {e.Message}" + e);
                return (MessageCode.UndefinedNetworkError, string.Empty);
            }
        }

        public void SetBaseAddress(string url) => HttpClient.BaseAddress = new Uri(url);

        public void ClearRequestHeaders()
        {
            HttpClient.DefaultRequestHeaders.Clear();
        }

        public void TryAddHeader(string name, string value)
        {
            HttpClient.DefaultRequestHeaders.TryAddWithoutValidation(name, value);
        }

        public void RemoveHeader(string name)
        {
            HttpClient.DefaultRequestHeaders.Remove(name);
        }
    }
}