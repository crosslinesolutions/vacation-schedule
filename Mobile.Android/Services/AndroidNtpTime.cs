﻿using Android.OS;
using Mobile.Core;
using Mobile.Core.Services.TimeService;
using System;

namespace Mobile.Android.Services
{
    public class AndroidNtpTime : NtpTimeService
    {
        private long ElapsedWhenNtpReceived;

        public override DateTime CurrentUTC
        {
            get
            {
                if (NtpTimeOnStartup == DateTime.MinValue)
                    return DateTime.UtcNow;
                else
                    return NtpTimeOnStartup.AddMilliseconds(SystemClock.ElapsedRealtime() - ElapsedWhenNtpReceived);
            }
        }


        public override DateTime CurrentTime
        {
            get
            {
                var timeToReturn = DateTime.Now;
                if (NtpTimeOnStartup == DateTime.MinValue)
                {
                    if (TryGetUTC().ConfigureAwait(false).GetAwaiter().GetResult() == MessageCode.Ok)
                    {
                        ElapsedWhenNtpReceived = SystemClock.ElapsedRealtime();
                        var calculatedTime = NtpTimeOnStartup.AddMilliseconds(SystemClock.ElapsedRealtime() - ElapsedWhenNtpReceived);
                        timeToReturn = CurrentTimezone != TimeSpan.MinValue ? calculatedTime.Add(CurrentTimezone) : DateTime.Now;
                    }
                }
                else
                {
                    var calculatedTime = NtpTimeOnStartup.AddMilliseconds(SystemClock.ElapsedRealtime() - ElapsedWhenNtpReceived);
                    timeToReturn = CurrentTimezone != TimeSpan.MinValue ? calculatedTime.Add(CurrentTimezone) : DateTime.Now;
                }
                return timeToReturn;
            }
        }
    }
}