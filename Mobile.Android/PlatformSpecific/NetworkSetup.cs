﻿using Android.App;
using Android.Content;
using Android.Net;

namespace Mobile.Android.PlatformSpecific
{
    public class NetworkSetup
    {
        public void RegisterNetworkCallBacks()
        {
            ConnectivityManager conn = (ConnectivityManager)Application.Context.GetSystemService(Context.ConnectivityService);
            NetworkRequest.Builder builder = new NetworkRequest.Builder();
            conn.RegisterNetworkCallback(builder.Build(), new NetworkCallbacks());
        }
    }
}