﻿using Android.Net;
using Mobile.Core.Services;
using MvvmCross;
using System;
using System.Collections.Generic;

namespace Mobile.Android.PlatformSpecific
{
    public class NetworkCallbacks : ConnectivityManager.NetworkCallback
    {
        private readonly List<string> AvailableNetworkAdapters = new List<string>(3);
        private readonly INetworkManager _networkManager;

        public NetworkCallbacks() => _networkManager = Mvx.IoCProvider.Resolve<INetworkManager>();

        public override void OnAvailable(Network network)
        {
            base.OnAvailable(network);
            AvailableNetworkAdapters.Add(network.ToString());
            CheckNetworkAdapters();
            Log.Event("[NetworkManager] [NetworkStatus] Available");
        }

        public override void OnCapabilitiesChanged(Network network, NetworkCapabilities networkCapabilities)
        {
            base.OnCapabilitiesChanged(network, networkCapabilities);
            Log.Event("[NetworkManager] [NetworkStatus] Network changed " + networkCapabilities.ToString());
        }

        public override void OnLosing(Network network, int maxMsToLive)
        {
            base.OnLosing(network, maxMsToLive);
            Log.Event("[NetworkManager] [NetworkStatus] Losing");
        }

        public override void OnLost(Network network)
        {
            base.OnLost(network);
            AvailableNetworkAdapters.Remove(network.ToString());
            CheckNetworkAdapters();
            Log.Event("[NetworkManager] [NetworkStatus] Lost");
        }

        public override void OnUnavailable()
        {
            base.OnUnavailable();
            Log.Event("[NetworkManager] [NetworkStatus] Unavailable");
        }

        private void CheckNetworkAdapters() => _networkManager.IsNetworkAvailable = AvailableNetworkAdapters.Count > 0 ? true : false;
    }
}