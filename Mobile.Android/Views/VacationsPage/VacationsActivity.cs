﻿using Android.App;
using Android.OS;
using Mobile.Core.ViewModels.EmployeePage;
using MvvmCross.Droid.Support.V7.AppCompat;

namespace Mobile.Android.Views.VacationsPage
{
    [Activity(Label = "Vacations")]
    public class VacationsActivity : MvxAppCompatActivity<EmployeePageVM>
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Activity_vacations);
        }
    }
}