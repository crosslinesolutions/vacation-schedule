﻿using Android.App;
using Android.OS;
using Mobile.Core.ViewModels.DepartmentPage;
using MvvmCross.Droid.Support.V7.AppCompat;

namespace Mobile.Android.Views.EmployeesPage
{
    [Activity(Label = "Employees")]
    public class EmplActivity : MvxAppCompatActivity<DepartmentPageVM>
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Activity_employees);
        }
    }
}