﻿using Android.App;
using Android.OS;
using Mobile.Core.ViewModels.MainPage;
using MvvmCross.Droid.Support.V7.AppCompat;

namespace Mobile.Android.Views
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : MvxAppCompatActivity<MainPageVM>
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_main);
        }
    }
}

