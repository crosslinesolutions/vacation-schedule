﻿using Android.App;
using Android.Content.PM;
using Android.OS;
using Mobile.Core.ViewModels.CompanyPage;
using MvvmCross.Droid.Support.V7.AppCompat;

namespace Mobile.Android.Views.DepartmentPage
{
    [Activity(LaunchMode = LaunchMode.SingleTop, ScreenOrientation = ScreenOrientation.Portrait, Label = "Departments")]
    public class DepartmentsActivity : MvxAppCompatActivity<CompanyPageVM>
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.Activity_Departments);
        }
    }
}