﻿using Android.App;
using Android.OS;
using Mobile.Core.ViewModels.VacationPage;
using MvvmCross.Droid.Support.V7.AppCompat;

namespace Mobile.Android.Views.VacationPage
{
    [Activity(Label = "Vacation")]
    public class VacationActivity : MvxAppCompatActivity<VacationPageVM>
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Activity_vacation);
        }
    }
}