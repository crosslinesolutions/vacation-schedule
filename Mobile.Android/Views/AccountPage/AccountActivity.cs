﻿using Android.App;
using Android.Content.PM;
using Android.OS;
using Mobile.Core.ViewModels.AccountPage;
using MvvmCross.Droid.Support.V7.AppCompat;

namespace Mobile.Android.Views.Fragments
{
    [Activity(LaunchMode = LaunchMode.SingleTop, ScreenOrientation = ScreenOrientation.Portrait, Label = "Companies")]
    public class AccountActivity : MvxAppCompatActivity<AccountPageVM>
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.Activity_account);
        }
    }
}