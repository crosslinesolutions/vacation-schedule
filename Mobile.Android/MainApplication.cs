﻿using Android.App;
using Android.Runtime;
using Mobile.Core;
using MvvmCross.Platforms.Android.Views;
using System;

namespace Mobile.Android
{
    [Application]
    public class MainApplication : MvxAndroidApplication<Setup, App>
    {
        public MainApplication(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
        {
        }
    }
}