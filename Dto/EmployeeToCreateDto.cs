﻿using System.ComponentModel.DataAnnotations;

namespace VacationSchedule.Dto
{
    public class EmployeeToCreateDto
    {
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string Position { get; set; }
        [Required]
        public int DepartamentId { get; set; }

        public EmployeeToCreateDto() { }

        public EmployeeToCreateDto(string firstName, string secondName, string position, int departmentId)
        {
            FirstName = firstName;
            LastName = secondName;
            Position = position;
            DepartamentId = departmentId;
        }
    }
}
