﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace VacationSchedule.Dto
{
    public class EmployeeDto
    {
        public int Id { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string Position { get; set; }
        [Required]
        public int DepartamentId { get; set; }

        public List<VacationDto> Vacations { get; set; }
    }
}
