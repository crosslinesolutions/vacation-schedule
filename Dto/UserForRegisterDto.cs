﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace VacationSchedule.Dto
{
    public class UserForRegisterDto
    {
        [Required]
        public string Username { get; set; }
        [Required]
        [StringLength(8, MinimumLength = 4, ErrorMessage = "Password must be a minimum of 4 and a maximum of 8 characters")]
        public string Password { get; set; }

        public UserForRegisterDto(string userName, string password)
        {
            Username = userName;
            Password = password;
        }
    }
}
