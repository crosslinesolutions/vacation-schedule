﻿using System.Collections.Generic;
using Dto;

namespace VacationSchedule.Dto
{
    public class UserDto : BaseEntity
    {
        public string Username { get; set; }
        public byte[] PasswordHash { get; set; }
        public byte[] PasswordSalt { get; set; }
        public List<CompanyDto> Companies { get; set; }
    }
}
