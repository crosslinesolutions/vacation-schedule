﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Dto;

namespace VacationSchedule.Dto
{
    public class VacationDto : BaseEntity
    {
        [Required]
        public int EmployeeId { get; set; }
        public EmployeeDto Employee { get; set; }
        [Required]
        public int VacatyionTypeId { get; set; }
        public VacatyionTypeDto VacatyionType { get; set; }

        [Required]
        public DateTime VacationStart { get ; set; }
        [Required]
        public DateTime VacationEnd { get; set; }
        public List<VacationDayDto> VacationDays { get; set; }
        public int TotalVacationDays { get; set; }
        public VacationStatus Status { get; set; }

        public VacationDto() { }

        public VacationDto(DateTime beginDate, DateTime endDate, int vacationTypeId, int empId)
        {
            EmployeeId = empId;
            VacatyionTypeId = vacationTypeId;
            VacationStart = beginDate;
            VacationEnd = endDate;
        }

        public enum VacationStatus
        {
            Creation,
            OnMatching,
            Agreed,
            Cancel,
            Close
        }
    }
}
