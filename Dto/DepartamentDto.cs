﻿namespace VacationSchedule.Dto
{
    public class DepartamentDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int CompanyId { get; set; }

        public DepartamentDto() { }

        public DepartamentDto(string departmentName, int companyId, string description)
        {
            Name = departmentName;
            CompanyId = companyId;
            Description = description;
        }
    }
}
