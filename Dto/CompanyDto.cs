﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace VacationSchedule.Dto
{
    public class CompanyDto
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public int UserId { get; set; }
        public IEnumerable<DepartamentDto> Departments { get; set; }

        //public CompanyDto()
        //{
        //    Departaments = new List<DepartamentDto>();
        //}
    }
}
