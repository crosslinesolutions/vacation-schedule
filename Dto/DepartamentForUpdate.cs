﻿using System.ComponentModel.DataAnnotations;

namespace VacationSchedule.Dto
{
    public class DepartamentForUpdate
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public int CompanyId { get; set; }
    }
}
