﻿namespace VacationSchedule.Dto
{
    public class BaseEntity
    {
        public int Id { get; set; }
    }
}
