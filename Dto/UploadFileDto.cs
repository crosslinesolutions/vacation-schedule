﻿namespace VacationSchedule.Dto
{
    public class UploadFileDto
    {
        public int DepartamentId { get; set; }
        public int CompanyId { get; set; }
    }
}
