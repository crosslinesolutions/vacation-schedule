﻿using System.Collections.Generic;

namespace VacationSchedule.Dto
{
    public class EmployeesListDto
    {
        public IEnumerable<EmployeeDto> EmployeesDto { get; set; }
    }
}
