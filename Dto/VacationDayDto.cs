﻿using System;
using VacationSchedule.Dto;

namespace Dto
{
    public class VacationDayDto : BaseEntity
    {
        public VacationDto Vacation { get; set; }
        public int VacationId { get; set; }
        public DateTime Dt { get; set; }
        public bool IsWorkDay { get; set; }
    }
}