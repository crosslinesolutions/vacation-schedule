﻿using Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace VacationSchedule.Dto
{
    public class DepartmentDtoResponse
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public CompanyDto Company { get; set; }
        public int CompanyId { get; set; }
        public List<EmployeeDto> Employees { get; set; }
        public int VacationDaysRule { get; set; }
        public int SameTimePeopleOnVacation { get; set; }
    }
}
