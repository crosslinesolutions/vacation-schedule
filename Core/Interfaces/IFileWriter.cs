﻿using System.Threading.Tasks;
using VacationSchedule.Core.Notifications;

namespace VacationSchedule.Core.Interfaces
{
    public interface IFileWriter
    {
        Task<string> WriteNotificationToFile(IVacationNotification notification);
    }
}