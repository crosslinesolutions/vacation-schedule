﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using VacationSchedule.Core.Entities;

namespace VacationSchedule.Core.Interfaces
{
    public interface ICompanyRepository
    {
       Task<Company> GetCompanyWithDepartamentsAsync(int departamentId);
    }
}
