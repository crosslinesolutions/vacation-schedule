﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

using VacationSchedule.Core.Entities;

namespace VacationSchedule.Core.Interfaces
{
    public interface IDepartamentRepository
    {
        Task<Department> GetDepartamentWhithEmployeesById(int departamentId);
    }
}
