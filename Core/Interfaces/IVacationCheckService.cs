﻿using System.Threading.Tasks;

namespace VacationSchedule.Core.Interfaces
{
    public interface IVacationCheckService
    {
        Task CheckVacations();
    }
}