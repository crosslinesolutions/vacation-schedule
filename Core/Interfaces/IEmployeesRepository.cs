﻿using System.Threading.Tasks;
using Core.Entities;
using VacationSchedule.Core.Entities;

namespace VacationSchedule.Core.Interfaces
{
    public interface IEmployeesRepository
    {
        Task<Employee> GetEmployeeById(int id);
    }
}
