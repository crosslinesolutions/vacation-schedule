﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Entities;
using VacationSchedule.Core.Entities;

namespace VacationSchedule.Core.Interfaces
{
    public interface IEmployeesService
    {
        Task LoadData(List<Employee> data);
    }
}
