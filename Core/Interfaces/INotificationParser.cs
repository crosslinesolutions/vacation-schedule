﻿using VacationSchedule.Core.Notifications;

namespace VacationSchedule.Core.Interfaces
{
    public interface INotificationParser
    {
        string ParseNotification(IVacationNotification notification);
    }
}