﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Core.Entities;
using VacationSchedule.Core.Entities;

namespace VacationSchedule.Core.Interfaces
{
    public interface IVacationRepository
    {
        Task<Vacation> GetVacationById(int id);
        List<Vacation> GetAllVacationStartNDaysBefore(int amoundDaysBefore);
        Task<List<Vacation>> GetAllVacationInDepartament(int departamentId);
    }
}
