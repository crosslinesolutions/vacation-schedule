﻿using System.Linq;
using System.Text;
using VacationSchedule.Core.Notifications;
using VacationSchedule.Core.Interfaces;

namespace VacationSchedule.Core.Services
{
    public class NotificationParser : INotificationParser
    {
        public string ParseNotification(IVacationNotification notification)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"Дата создания: {notification.CreateDate.ToShortDateString()}");
            if (notification.Vacations != null && notification.Vacations.Any())
            {
                sb.AppendLine("Сотрудники: ");
                notification.Vacations.Select(x => sb.AppendLine(x.ToString()));
            }
            else
            {
                sb.AppendLine($"Нет ближайших плановых отпусков!");
            }
            return sb.ToString();
        }
    }
}
