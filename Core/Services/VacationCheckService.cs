﻿using Core.Entities;
using Microsoft.Extensions.Configuration;
using Serilog;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VacationSchedule.Core.Entities;
using VacationSchedule.Core.Interfaces;
using VacationSchedule.Core.NotificationCenter;
using VacationSchedule.Core.Notifications;

namespace VacationSchedule.Core.Services
{
    /// <summary>
    /// Сервис для поиска работников у которых отпуск начинается в течении следующих n дней
    /// </summary>
    public class VacationCheckService : IVacationCheckService
    {
        private readonly IConfiguration _configuration;
        private readonly IVacationRepository _vacationRepository;
        private readonly INotificationCenter _notificationCenter;

        public VacationCheckService(IConfiguration configuration, IVacationRepository vacationRepository, INotificationCenter notificationCenter)
        {
            _configuration = configuration;
            _vacationRepository = vacationRepository;
            _notificationCenter = notificationCenter;
        }

        public async Task CheckVacations()
        {
            var vacations = CheckVacationsNDaysBefore(_configuration.GetValue<int>("DaysBeforeVacation"));
            var notification = CreateNotification(vacations);
            await SendNotification(notification);
        }

        private List<Vacation> CheckVacationsNDaysBefore(int daysBefore)
        {
            return _vacationRepository.GetAllVacationStartNDaysBefore(daysBefore);
        }

        private VacationNotification CreateNotification(List<Vacation> vacations)
        {
            return new VacationNotification(vacations.Select(x => new VacationInfo(x)));
        }

        private async Task SendNotification(VacationNotification notification)
        {
            Log.Debug("Send @{notification}", notification);
            await _notificationCenter.Send<IVacationNotification>(notification);
        }
    }
}
