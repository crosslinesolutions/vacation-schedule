﻿using Microsoft.Extensions.Configuration;
using Serilog;
using System;
using System.IO;
using System.Threading.Tasks;
using VacationSchedule.Core.Interfaces;
using VacationSchedule.Core.Notifications;

namespace VacationSchedule.Core.Services
{
    public class FileWriter : IFileWriter
    {
        private readonly IConfiguration _configuration;
        private readonly INotificationParser _notificationParser;

        public FileWriter(IConfiguration configuration, INotificationParser notificationParser)
        {
            _configuration = configuration;
            _notificationParser = notificationParser;
        }

        public async Task<string> WriteNotificationToFile(IVacationNotification notification)
        {
            var path = GenerateFilePath();
            var parsedNotification = _notificationParser.ParseNotification(notification);
            using (StreamWriter streamWriter = new StreamWriter(path))
            {
                Log.Information($"Notification writed in {path}");
                await streamWriter.WriteAsync(parsedNotification);
            }
            return path;
        }

        private string GenerateFilePath()
        {
            var fileName = DateTime.Now.ToString("yyyyMMddhhmmss") + _configuration.GetValue<string>("FileExtantion");
            var filePath = _configuration.GetValue<string>("FilePath") ;
            return Path.Combine(filePath, fileName);
        }
    }
}
