﻿using MassTransit;
using Microsoft.Extensions.Configuration;
using Serilog;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace VacationSchedule.Core.NotificationCenter
{
    /// <summary>
    /// Класс для отправки сообщений в очередь
    /// </summary>
    public class NotificationCenter : INotificationCenter
    {
        private readonly ISendEndpointProvider _sendEndpointProvider;
        private readonly Uri _queueUri;

        public NotificationCenter(ISendEndpointProvider sendEndpointProvider, IConfiguration configuration)
        {
            _sendEndpointProvider = sendEndpointProvider;
            _queueUri = new Uri($"queue:{configuration.GetValue<string>("RabbitQueueName")}");
        }

        public async Task Send<T>(T message) where T : class
        {
            try
            {
                var endpoint = await _sendEndpointProvider.GetSendEndpoint(_queueUri);
                await endpoint.Send(message);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Send to Rabbit error");
            }
        }
    }
}
