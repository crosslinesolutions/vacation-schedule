﻿using System.Threading.Tasks;

namespace VacationSchedule.Core.NotificationCenter
{
    public interface INotificationCenter
    {
        Task Send<T>(T message) where T : class;
    }
}