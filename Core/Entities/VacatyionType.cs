﻿using Core.Entities;

namespace VacationSchedule.Core.Entities
{
    public class VacationType : BaseEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }

    }
}
