﻿using System.Collections.Generic;

namespace VacationSchedule.Core.Entities
{
    public class Employee : BaseEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Position { get; set; }
        public Department Departament { get; set; }
        public int DepartamentId { get; set; }
        public List<Vacation> Vacations { get; set; }
    }
}
