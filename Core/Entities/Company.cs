﻿using System.Collections.Generic;
using System.Text.Json.Serialization;
using Core.Entities;

namespace VacationSchedule.Core.Entities
{
    public class Company : BaseEntity
    {
        public string Name { get; set; }
        public int UserId { get; set; }
        [JsonIgnore]
        public User User { get; set; }
        public List<Department> Departments { get; set; }
    }
}
