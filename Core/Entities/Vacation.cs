﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace VacationSchedule.Core.Entities
{
    public class Vacation : BaseEntity
    {
        private int _totalVacationDays;
        public int EmployeeId { get; set; }
        public Employee Employee { get; set; }
        public int VacatyionTypeId { get; set; }
        public VacationType VacationType { get; set; }
        public DateTime VacationStart { get; set; }
        public DateTime VacationEnd { get; set; }
        public List<VacationDay> VacationDays { get; set; }

        public int TotalVacationDays
        {
            get => this.VacationDays.Where(d=>d.IsWorkDay == true).ToList().Count();
            set => _totalVacationDays = value;
        }

        public VacationStatus Status { get; set; }
        public enum VacationStatus
        {
            Creation,
            OnMatching,
            Agreed,
            Cancel,
            Close
        }

    }
}
