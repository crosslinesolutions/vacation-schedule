﻿using System.Collections.Generic;
using System.Text.Json.Serialization;


namespace VacationSchedule.Core.Entities
{
    public class Department : BaseEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        [JsonIgnore]
        public Company Company { get; set; }
        public int CompanyId { get; set; }
        public List<Employee> Employees { get; set; }
        public int VacationDaysRule { get; set; }
        public int SameTimePeopleOnVacation { get; set; }

    }
}
