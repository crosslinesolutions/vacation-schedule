﻿using System;
using System.Reflection.Metadata;
using VacationSchedule.Core.Entities;

namespace VacationSchedule.Core.Entities
{
    public class VacationDay : BaseEntity
    {
        public Vacation Vacation { get; set; }
        public int VacationId { get; set; }
        public DateTime Dt { get; set; }
        public bool IsWorkDay { get; set; }
    }
}