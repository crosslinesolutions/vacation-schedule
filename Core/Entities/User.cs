﻿using System;
using System.Collections.Generic;
using System.Text;
using VacationSchedule.Core.Entities;

namespace Core.Entities
{
    public class User : BaseEntity
    {
        public string Username { get; set; }
        public byte[] PasswordHash { get; set; }
        public byte[] PasswordSalt { get; set; }
        public List<Company> Companies { get; set; }
    }
}
