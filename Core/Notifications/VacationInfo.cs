﻿using System;
using System.Collections.Generic;
using System.Text;
using VacationSchedule.Core.Entities;

namespace VacationSchedule.Core.Notifications
{
    public class VacationInfo
    {
        public Employee Employee { get; set; }
        public DateTime StartDate { get; set; }
        public string LeftBeforeVacation 
        { 
            get 
            { 
                return $"Осталось дней до отпуска: {StartDate.Subtract(DateTime.Now).Days}"; 
            } 
        }

        public VacationInfo(Vacation vacation)
        {
            Employee = vacation.Employee;
            StartDate = vacation.VacationStart;
        }

        public override string ToString()
        {
            return $"Сотрудник: {Employee.FirstName} {Employee.LastName}, Отдел: {Employee.Departament.Name}, Начало отпуска: {StartDate.ToShortDateString()}, Статус: {LeftBeforeVacation}";
        }
    }
}
