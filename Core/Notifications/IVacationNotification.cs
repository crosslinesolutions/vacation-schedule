﻿using System;
using System.Collections.Generic;

namespace VacationSchedule.Core.Notifications
{
    public interface IVacationNotification
    {
        DateTime CreateDate { get; set; }
        IEnumerable<VacationInfo> Vacations { get; set; }
    }
}