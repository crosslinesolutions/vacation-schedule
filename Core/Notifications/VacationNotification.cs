﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VacationSchedule.Core.Notifications
{
    public class VacationNotification : IVacationNotification
    {
        public IEnumerable<VacationInfo> Vacations { get; set; }
        public DateTime CreateDate { get; set; }

        public VacationNotification(IEnumerable<VacationInfo> vacations)
        {
            Vacations = vacations;
            CreateDate = DateTime.Now;
        }
    }
}
