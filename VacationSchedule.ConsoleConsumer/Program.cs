﻿using MassTransit;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using System;
using System.IO;
using System.Reflection;
using VacationSchedule.Core.MassTransit;

namespace VacationSchedule.ConsoleConsumer
{
    class Program
    {
        static void Main(string[] args)
        {
            var serviceProvider = ConfigureServices().BuildServiceProvider();
            Console.ReadKey();
        }

        private static IServiceCollection ConfigureServices()
        {
            IServiceCollection services = new ServiceCollection();

            var configuration = new ConfigurationBuilder()
                .SetBasePath(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location))
                .AddJsonFile("appsettings.json", false)
                .Build();

            services.AddSingleton<IConfiguration>(configuration);

            //MassTransit
            services.Configure<MassTransitConfiguration>(configuration.GetSection("MassTransit"));
            services.AddMassTransit(x =>
            {
                x.AddBus(isp => Bus.Factory.CreateUsingRabbitMq(cfg =>
                {
                    var hostConfig = isp.GetRequiredService<IOptions<MassTransitConfiguration>>().Value;
                    cfg.Host(new Uri(hostConfig.RabbitMqAddress + "/"), h =>
                    {
                        h.Username(hostConfig.UserName);
                        h.Password(hostConfig.Password);
                    });
                }));
            });
            services.AddMassTransitHostedService();

            return services;
        }
    }
}
