﻿using Quartz;
using System.Threading.Tasks;
using VacationSchedule.Core.Interfaces;

namespace VacationSchedule.Scheduling.Jobs
{
    /// <summary>
    /// Джоба для отправки оповещений о приближении отпуска сотрудников
    /// </summary>
    [DisallowConcurrentExecution]
    public class VacationNotificationJob : BaseJob
    {
        private const string JobName = nameof(VacationNotificationJob);
        private readonly IVacationCheckService _vacationCheckService;
        public VacationNotificationJob(IVacationCheckService vacationCheckService)
        {
            _vacationCheckService = vacationCheckService;
        }

        protected override Task InternalExecute(IJobExecutionContext context)
        {
            _vacationCheckService.CheckVacations();
            return Task.CompletedTask;
        }

        public static void Configure(IScheduler scheduler)
        {
            string cronMask = "0 0 0 * * ?";

            var jobTrigger = TriggerBuilder.Create()
                    .WithIdentity($"Cron trigger for job {JobName}")
                    .WithCronSchedule(cronMask)
                    .StartNow()
                    .Build();

            var jobDetails = JobBuilder.Create<VacationNotificationJob>()
                .WithIdentity(JobName)
                .Build();

            scheduler.ScheduleJob(jobDetails, jobTrigger);
        }
    }
}
