﻿using Quartz;
using Serilog;
using Serilog.Context;
using System;
using System.Threading.Tasks;

namespace VacationSchedule.Scheduling.Jobs
{
    public abstract class BaseJob : IJob
    {
        public async Task Execute(IJobExecutionContext context)
        {
            string jobName = context.JobDetail.Key.Name;

            using (LogContext.PushProperty(nameof(jobName), jobName))
            using (Log.Logger.BeginTimedOperation($"{jobName} executed time"))
            {
                try
                {
                    await InternalExecute(context);
                }
                catch (Exception ex)
                {
                    Log.Error(ex, "Job exception");
                }
            }
        }

        protected abstract Task InternalExecute(IJobExecutionContext context);
    }
}
