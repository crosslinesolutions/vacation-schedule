﻿using MassTransit;
using Quartz;
using System;
using System.Collections.Generic;
using System.Text;
using Topshelf;
using VacationSchedule.Scheduling.Jobs;

namespace VacationSchedule.Scheduling
{
    /// <summary>
    /// Класс для хостинга планировщика
    /// </summary>
    public class MainService : ServiceControl
    {
        private readonly IScheduler _scheduler;
        private readonly IBusControl _busControl;

        public MainService(IScheduler scheduler, IBusControl busControl)
        {
            _scheduler = scheduler ?? throw new ArgumentNullException(nameof(scheduler));
            _busControl = busControl ?? throw new ArgumentNullException(nameof(busControl));
        }

        public bool Start(HostControl hostControl)
        {
            VacationNotificationJob.Configure(_scheduler);

            _scheduler.Start().ConfigureAwait(false).GetAwaiter().GetResult();
            _busControl.Start();

            return true;
        }

        public bool Stop(HostControl hostControl)
        {
            _scheduler.Shutdown().ConfigureAwait(false).GetAwaiter().GetResult();
            _busControl?.Stop(TimeSpan.FromSeconds(30));

            return true;
        }
    }
}
