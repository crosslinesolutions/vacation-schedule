﻿using MassTransit;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Quartz.Impl;
using Quartz.Logging;
using Quartz.Spi;
using Serilog;
using System;
using System.IO;
using System.Reflection;
using Topshelf;
using VacationSchedule.Core.Interfaces;
using VacationSchedule.Core.MassTransit;
using VacationSchedule.Core.NotificationCenter;
using VacationSchedule.Core.Services;
using VacationSchedule.Infrastructure.Data;
using VacationSchedule.Scheduling.Jobs;
using VacationSchedule.Scheduling.Quartz;

namespace VacationSchedule.Scheduling
{
    class Program
    {
        static void Main(string[] args)
        {
            var serviceProvider = ConfigureServices().BuildServiceProvider();
            try
            {
                HostFactory.Run(hostConfigurator =>
                {
                    //Serilog 
                    Log.Logger = new LoggerConfiguration()
                        .ReadFrom.Configuration(serviceProvider.GetService<IConfiguration>())
                        .CreateLogger();

                    //Serilog adapter for Quartz
                    LogProvider.SetCurrentLogProvider(new SerilogLogProviderAdapter(Log.Logger));

                    hostConfigurator.Service(s => serviceProvider.GetService<MainService>());
                });
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Ошибка!");
            }
            finally
            {
                serviceProvider?.Dispose();
            }
        }

        private static IServiceCollection ConfigureServices()
        {
            IServiceCollection services = new ServiceCollection();

            var configuration = new ConfigurationBuilder()
                .SetBasePath(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location))
                .AddJsonFile("appsettings.json", false)
                .Build();

            services.AddSingleton<IConfiguration>(configuration);

            services.AddDbContext<VacationContext>(c =>
            {
                c.UseInMemoryDatabase("VacationsDb");
                //c.UseSqlServer(configuration.GetConnectionString("VacationsConnection"));
            });
            services.AddScoped<IVacationRepository, VacationRepository>();
            services.AddSingleton<IVacationCheckService, VacationCheckService>();
            services.AddSingleton<INotificationCenter, NotificationCenter>();

            //Topshelf
            services.AddSingleton<MainService>();

            //Quartz
            services.AddSingleton<IJobFactory>(isp => new QuartzJobFactory(isp));
            services.AddSingleton(provider =>
            {
                var scheduler = new StdSchedulerFactory().GetScheduler().ConfigureAwait(false).GetAwaiter().GetResult();
                scheduler.JobFactory = provider.GetService<IJobFactory>();
                return scheduler;
            });
            //Job registration (обязательно!)
            services.AddScoped<VacationNotificationJob>();

            //MassTransit
            services.Configure<MassTransitConfiguration>(configuration.GetSection("MassTransit"));
            services.AddMassTransit(x =>
            {
                x.AddBus(isp => Bus.Factory.CreateUsingRabbitMq(cfg =>
                {
                    var hostConfig = isp.GetRequiredService<IOptions<MassTransitConfiguration>>().Value;
                    cfg.Host(new Uri(hostConfig.RabbitMqAddress + "/"), h =>
                    {
                        h.Username(hostConfig.UserName);
                        h.Password(hostConfig.Password);
                    });
                }));
            });
            services.AddMassTransitHostedService();

            return services;
        }
    }
}
