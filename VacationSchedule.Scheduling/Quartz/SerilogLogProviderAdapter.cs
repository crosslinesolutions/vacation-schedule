﻿using Quartz.Logging;
using Serilog;
using Serilog.Events;
using System;

namespace VacationSchedule.Scheduling.Quartz
{
    public class SerilogLogProviderAdapter : ILogProvider
    {
        private readonly ILogger _logger;

        public SerilogLogProviderAdapter(ILogger logger)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public Logger GetLogger(string name)
        {
            return (level, func, exception, parameters) =>
            {
                var template = func == null ? string.Empty : func();
                _logger.Write(level.Map(), exception, template, parameters);
                return true;
            };
        }

        public IDisposable OpenMappedContext(string key, string value)
        {
            throw new NotImplementedException();
        }

        public IDisposable OpenNestedContext(string message)
        {
            throw new NotImplementedException();
        }
    }

    public static class MapExtensions
    {
        public static LogEventLevel Map(this LogLevel level)
        {
            switch (level)
            {
                case LogLevel.Debug:
                    return LogEventLevel.Debug;

                case LogLevel.Error:
                    return LogEventLevel.Error;

                case LogLevel.Fatal:
                    return LogEventLevel.Fatal;

                case LogLevel.Info:
                    return LogEventLevel.Information;

                case LogLevel.Trace:
                    return LogEventLevel.Verbose;

                case LogLevel.Warn:
                    return LogEventLevel.Warning;

                default:
                    throw new ArgumentOutOfRangeException(nameof(level), level, null);
            }
        }
    }
}